export interface IEmailNotificationsService<T> {
  sendNotification(data: T): Promise<void>;
}
