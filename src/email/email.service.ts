import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { createTransport, Transporter } from 'nodemailer';
import SMTPPool from 'nodemailer/lib/smtp-pool';
import { TransportConfig } from './types/TransportConfig';

@Injectable()
export class EmailService implements OnModuleInit {
  private readonly config: TransportConfig;
  private pooledTransport: Transporter;

  constructor(private readonly configService: ConfigService) {
    this.config = {
      name: this.configService.get('MAIL_NAME'),
      username: this.configService.get('MAIL_USERNAME'),
      password: this.configService.get('MAIL_PASSWORD'),
      host: this.configService.get('MAIL_SMTP_HOST'),
      secure: this.configService.get('MAIL_SMTP_SECURE') === 'true',
    };

    console.info('Email config:', this.config);
  }

  public get transporter(): Readonly<Transporter> {
    return this.pooledTransport;
  }

  async onModuleInit() {
    this.pooledTransport = createTransport(
      new SMTPPool({
        pool: true,
        // name: this.config.name,
        host: this.config.host,
        // port is selected automatically based on this setting
        secure: this.config.secure,
        // determines whether TLS MUST be available
        requireTLS: this.config.secure,
        auth: {
          user: this.config.username,
          pass: this.config.password,
        },
        tls: {
          rejectUnauthorized: !(process.env.NODE_ENV === 'development'),
        },
        logger: this.configService.get('NODEMAILER_LOGS') === 'true',
        debug: this.configService.get('NODEMAILER_LOGS') === 'true',
      }),
      { from: this.config.username } as SMTPPool.Options,
    );

    try {
      console.info('Email module verify connection...');
      await this.pooledTransport.verify();
      console.info('Email module connection ready.');
    } catch (e) {
      console.error('Email module connection failed.');
      throw e;
    }
  }
}
