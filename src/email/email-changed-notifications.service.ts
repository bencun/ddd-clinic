import { Injectable } from '@nestjs/common';
import { EmailService } from './email.service';
import { IEmailNotificationsService } from './interfaces/IEmailNotificationsService';
import { EmailChangedNotificationData } from './types/EmailChangedNotificationData';
import sanitizeHtml from 'sanitize-html';

@Injectable()
export class EmailChangedNotificationsService
  implements IEmailNotificationsService<EmailChangedNotificationData>
{
  constructor(private readonly emailService: EmailService) {}

  // TODO massive improvements needed here!
  async sendNotification(data: EmailChangedNotificationData) {
    const html = `
      <div>
        <p>Dear ${data.to.name},</p>
        <p>Your email has been changed to ${data.to.email}.</p>
      </div>
    `;
    const text = sanitizeHtml(html, { allowedTags: [], allowedAttributes: {} });
    try {
      await this.emailService.transporter.sendMail({
        subject: `DDD Clinic Account Information`,
        to: data.to.email,
        html, // replace with rich html
        text, // replace with stripped html version
      });
      console.info('Sending successful');
    } catch (e) {
      console.error('Sending failed', e);
    }
  }
}
