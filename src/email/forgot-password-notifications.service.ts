import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EmailService } from './email.service';
import { IEmailNotificationsService } from './interfaces/IEmailNotificationsService';
import { ForgotPasswordNotificationData } from './types/ForgotPasswordNotificationData';
import sanitizeHtml from 'sanitize-html';

@Injectable()
export class ForgotPasswordNotificationsService
  implements IEmailNotificationsService<ForgotPasswordNotificationData>
{
  constructor(
    private readonly emailService: EmailService,
    private readonly configService: ConfigService,
  ) {}

  // TODO massive improvements needed here!
  async sendNotification(data: ForgotPasswordNotificationData) {
    const href = `${this.configService.get('CLIENT_URL')}activation/reset/${data.resetToken}`;
    const html = `
      <div>
        <p>You have requested a password reset. Please, try resetting your password <a href="${href}">here.</a></p>

        <p>Copy and paste the following URL if you cannot access the link above by clicking it:</p>
        <p>${href}</p>
      </div>
    `;
    const text = sanitizeHtml(html, { allowedTags: [], allowedAttributes: {} });
    try {
      await this.emailService.transporter.sendMail({
        subject: `DDD Clinic Password Reset`,
        to: data.to,
        html, // replace with rich html
        text, // replace with stripped html version
      });
      console.info('Sending successful');
    } catch (e) {
      console.error('Sending failed', e);
    }
  }
}
