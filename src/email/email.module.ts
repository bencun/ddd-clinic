import { Module } from '@nestjs/common';
import { ActivationNotificationsService } from './activation-notifications.service';
import { EmailChangedNotificationsService } from './email-changed-notifications.service';
import { EmailService } from './email.service';
import { ForgotPasswordNotificationsService } from './forgot-password-notifications.service';

@Module({
  imports: [],
  providers: [
    EmailService,
    ActivationNotificationsService,
    EmailChangedNotificationsService,
    ForgotPasswordNotificationsService,
  ],
  controllers: [],
  exports: [
    ActivationNotificationsService,
    EmailChangedNotificationsService,
    ForgotPasswordNotificationsService,
  ],
})
export class EmailModule {}
