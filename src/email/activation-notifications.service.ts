import { Injectable } from '@nestjs/common';
import { EmailService } from './email.service';
import { IEmailNotificationsService } from './interfaces/IEmailNotificationsService';
import { ActivationNotificationData } from './types/ActivationNotificationData';
import { ConfigService } from '@nestjs/config';
import sanitizeHtml from 'sanitize-html';

@Injectable()
export class ActivationNotificationsService
  implements IEmailNotificationsService<ActivationNotificationData>
{
  constructor(
    private readonly emailService: EmailService,
    private readonly configService: ConfigService,
  ) {}

  // TODO massive improvements needed here!
  async sendNotification(data: ActivationNotificationData) {
    const href = `${this.configService.get('CLIENT_URL')}activation/${data.token}`;
    const html = `
      <div>
        <p>An account was created for you on DDD Clinic. Please <a href="${href}">activate</a> your account</p>

        <p>Copy and paste the following URL if you cannot access the link above by clicking it:</p>
        <p>${href}</p>
      </div>
    `;
    const text = sanitizeHtml(html, { allowedTags: [], allowedAttributes: {} });
    try {
      await this.emailService.transporter.sendMail({
        subject: `Activate your account on DDD Clinic`,
        to: data.to,
        html, // replace with rich html
        text, // replace with stripped html version
      });
      console.info('Sending successful');
    } catch (e) {
      console.error('Sending failed', e);
    }
  }
}
