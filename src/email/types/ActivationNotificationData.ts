export type ActivationNotificationData = {
  to: string;
  token: string;
};
