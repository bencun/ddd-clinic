export type TransportConfig = {
  host: string;
  name: string;
  username: string;
  password: string;
  secure: boolean;
};
