export type ForgotPasswordNotificationData = {
  resetToken: string;
  to: string;
  resetPassword: boolean;
};
