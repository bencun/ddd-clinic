export type EmailChangedNotificationData = {
  to: { name: string; email: string };
};
