import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './database/database.module';
import { ScheduleModule } from '@nestjs/schedule';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';
import { UsersModule } from './users/users.module';
import { EmailModule } from './email/email.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { EventsModule } from './events/events.module';
import { ClinicModule } from './subdomains/clinic/clinic.module';
import { PatientsModule } from './subdomains/patients/patients.module';
import { SchedulingModule } from './subdomains/scheduling/scheduling.module';
import { ExaminationModule } from './subdomains/examinations/examinations.module';
import { GatewayModule } from './gateway/gateway.module';
@Module({
  imports: [
    AuthModule,
    // Initialize ConfigModule as a @Global() module
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: [
        // use this one if you want to specify your own variables instead of relying on the default .development.env file
        'local.development.env',
        // the default development env file in the repo
        '.development.env',
        // the production env file
        '.env',
      ],
    }),
    // Reminder: DatabaseModule is @Global()
    DatabaseModule,
    // Schedule module for cron jobs
    ScheduleModule.forRoot(),
    // Throttler module
    ThrottlerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        ttl: +configService.get<number>('THROTTLER_TTL') || 60,
        limit: +configService.get<number>('THROTTLER_LIMIT') || 60,
      }),
    }),
    // EventEmitter module and the helper module
    EventEmitterModule.forRoot({ maxListeners: 100 }),
    EventsModule,
    GatewayModule,
    // Other app modules
    UsersModule,
    EmailModule,
    ClinicModule,
    PatientsModule,
    SchedulingModule,
    ExaminationModule,
  ],
  controllers: [],
  providers: [{ provide: APP_GUARD, useClass: ThrottlerGuard }],
})
export class AppModule {}
