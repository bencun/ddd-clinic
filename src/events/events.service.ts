import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { DomainEvent } from 'src/shared/ddd/DomainEvent';

@Injectable()
export class EventsService {
  constructor(private readonly eventEmitter: EventEmitter2) {}

  static CreateHandler<T extends DomainEvent, P = any>(
    event: T,
    handler: (event: T) => Promise<P>,
  ) {
    return {
      async execute() {
        if (event.transactional) {
          // exceptions are not disregarded
          return await handler(event);
        } else {
          // disregard the exceptions for non-transactional events
          handler(event).catch(() => undefined);
        }
      },
    };
  }

  static async CreateAndExecuteHandler<T extends DomainEvent, P = any>(
    event: T,
    handler: (event: T) => Promise<P>,
  ) {
    return await EventsService.CreateHandler(event, handler).execute();
  }

  async emit(domainEvent: DomainEvent) {
    return await this.eventEmitter.emitAsync(domainEvent.token, domainEvent);
  }

  async emitAll(domainEvents: Readonly<DomainEvent[]>) {
    for (const e of domainEvents) {
      await this.emit(e);
    }
  }
}
