import { Module } from '@nestjs/common';
import { ClinicController } from './http/clinic.controller';
import { ClinicRepositoryProvider } from './persistence/ClinicRepository/Provider';
import { ClinicSerializer } from './serializers/ClinicSerializer';
import { GetClinic } from './useCases/GetClinic';
import { SetClinicPreferences } from './useCases/SetClinicPreferences';

@Module({
  controllers: [ClinicController],
  providers: [ClinicRepositoryProvider, GetClinic, SetClinicPreferences, ClinicSerializer],
  exports: [GetClinic],
})
export class ClinicModule {}
