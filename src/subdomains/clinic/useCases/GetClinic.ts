import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Clinic } from '../entities/Clinic';
import { IClinicRepository } from '../persistence/ClinicRepository/IClinicRepository';

@Injectable()
export class GetClinic implements IUseCase<Clinic> {
  constructor(private readonly repository: IClinicRepository) {}

  async execute(): Promise<Clinic> {
    return await this.repository.loadClinic();
  }
}
