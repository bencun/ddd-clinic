import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { PreferenceObject, Clinic } from '../entities/Clinic';
import { IClinicRepository } from '../persistence/ClinicRepository/IClinicRepository';

@Injectable()
export class SetClinicPreferences implements IUseCase<Clinic> {
  constructor(private readonly repository: IClinicRepository) {}

  async execute(preferences: PreferenceObject[]): Promise<Clinic> {
    const clinic = await this.repository.loadClinic();
    for (const p of preferences) {
      clinic.setPreference(p.key, p.value);
    }
    await this.repository.persistClinic(clinic);
    return clinic;
  }
}
