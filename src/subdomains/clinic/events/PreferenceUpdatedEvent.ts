import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { DomainEvent } from 'src/shared/ddd/DomainEvent';

export class PreferenceUpdatedEvent extends DomainEvent {
  constructor(public readonly key: string) {
    super();
  }
  public get token(): DomainEventToken {
    return DomainEventToken.PREFERENCE_UPDATED;
  }
}
