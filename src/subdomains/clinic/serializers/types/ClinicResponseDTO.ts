export type ClinicResponseDTO = {
  preferences: {
    key: string;
    value: any;
  }[];
};
