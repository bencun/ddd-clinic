import { Injectable } from '@nestjs/common';
import { IEntitySerializer } from 'src/shared/ddd/IEntitySerializer';
import { Clinic } from '../entities/Clinic';
import { ClinicResponseDTO } from './types/ClinicResponseDTO';

@Injectable()
export class ClinicSerializer implements IEntitySerializer<Clinic, ClinicResponseDTO> {
  serialize(entity: Clinic): ClinicResponseDTO {
    const prefsArray = entity.getPreferencesAsObject();
    const serialized: ClinicResponseDTO = {
      preferences: prefsArray.map((p) => ({ key: Object.keys(p)[0], value: Object.values(p)[0] })),
    };
    return serialized;
  }
}
