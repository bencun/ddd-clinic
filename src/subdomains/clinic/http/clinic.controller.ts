import { Role } from '.prisma/client';
import { BadRequestException, Body, Controller, Get, Post } from '@nestjs/common';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { ALL_ROLES } from 'src/auth/types/constants';
import { ClinicSerializer } from '../serializers/ClinicSerializer';
import { GetClinic } from '../useCases/GetClinic';
import { SetClinicPreferences } from '../useCases/SetClinicPreferences';
import { SetPreferencesDTO } from './dto/SetPreferencesDTO';

@Controller('clinic')
export class ClinicController {
  constructor(
    private readonly getClinic: GetClinic,
    private readonly setClinicPreferences: SetClinicPreferences,
    private readonly clinicSerializer: ClinicSerializer,
  ) {}

  @Roles(...ALL_ROLES)
  @Get('getPreferences')
  async getPreferencesController() {
    return this.clinicSerializer.serialize(await this.getClinic.execute());
  }

  @Roles(Role.ADMIN)
  @Post('setPreferences')
  async setPreferencesController(@Body() { preferences }: SetPreferencesDTO) {
    try {
      return this.clinicSerializer.serialize(await this.setClinicPreferences.execute(preferences));
    } catch (e) {
      throw new BadRequestException((e as Error).message);
    }
  }
}
