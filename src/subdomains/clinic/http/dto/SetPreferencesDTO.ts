import { Type } from 'class-transformer';
import { ArrayNotEmpty, IsArray, IsDefined, IsEnum, ValidateNested } from 'class-validator';
import { ClinicPreferenceName } from '../../constants/ClinicPreferenceName';

class PreferencesDTO {
  @IsEnum(ClinicPreferenceName)
  key: ClinicPreferenceName;

  @IsDefined()
  value: any;
}

export class SetPreferencesDTO {
  @IsArray()
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => PreferencesDTO)
  preferences: PreferencesDTO[];
}
