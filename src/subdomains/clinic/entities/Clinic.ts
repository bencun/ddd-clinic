import { AggregateRoot } from 'src/shared/ddd/AggregateRoot';
import { PreferenceUpdatedEvent } from 'src/subdomains/clinic/events/PreferenceUpdatedEvent';
import { ClinicPreferenceDefaults } from '../constants/ClinicePreferenceDefaults';
import { ClinicPreferenceName } from '../constants/ClinicPreferenceName';

export type PreferenceObject = { key: ClinicPreferenceName; value: any };
export type RawPreferenceObject = Record<string, any>;

export class Clinic extends AggregateRoot {
  public readonly preferences: Map<ClinicPreferenceName, any> = new Map<
    ClinicPreferenceName,
    any
  >();

  constructor(preferencesArray: PreferenceObject[]) {
    super(1);
    // set the defaults first
    Object.keys(ClinicPreferenceDefaults).forEach((key) => {
      this.setPreference(key as ClinicPreferenceName, ClinicPreferenceDefaults[key]);
    });
    // set the provided preferences
    preferencesArray.forEach(({ key, value }) => {
      this.setPreference(key, value);
    });
  }

  public setPreference(key: ClinicPreferenceName, value: any) {
    const validation = this.invokeValidator(key, value);
    if (validation) {
      this.preferences.set(key, value);
      this.addEvent(new PreferenceUpdatedEvent(key));
    } else {
      throw new Error(`Invalid preference value for ${key}:${value}`);
    }
  }

  public getPreference(key: ClinicPreferenceName) {
    return this.preferences.get(key) || null;
  }

  public getPreferencesAsObject(): RawPreferenceObject[] {
    const entriesToArray = [...this.preferences.entries()];
    const preferencesArray: RawPreferenceObject[] = [];
    entriesToArray.forEach(([key, value]) => preferencesArray.push({ [key]: value }));
    return preferencesArray;
  }

  private invokeValidator(key: string, value: any) {
    return (
      (key === ClinicPreferenceName.CLINIC_NAME && this.validateClinicName(value)) ||
      (key === ClinicPreferenceName.CLINIC_ADDRESS && this.validateClinicAddress(value)) ||
      (key === ClinicPreferenceName.CLINIC_PHONE_NUMBER && this.validateClinicPhoneNumber(value)) ||
      (key === ClinicPreferenceName.CLINIC_EMAIL && this.validateClinicEmail(value)) ||
      (key === ClinicPreferenceName.CLINIC_WEBSITE && this.validateClinicWebsite(value)) ||
      (key === ClinicPreferenceName.CLINIC_NUMBER_OF_ROOMS &&
        this.validateClinicNumberOfRooms(value))
    );
  }

  private validateClinicName(value: any) {
    return typeof value === 'string' && value.trim().length > 0;
  }

  private validateClinicAddress(value: any) {
    return typeof value === 'string' && value.trim().length > 0;
  }

  private validateClinicPhoneNumber(value: any) {
    return typeof value === 'string' && value.trim().length > 0;
  }

  private validateClinicEmail(value: any) {
    return typeof value === 'string' && value.trim().length > 0;
  }

  private validateClinicWebsite(value: any) {
    return typeof value === 'string' && value.trim().length > 0;
  }

  private validateClinicNumberOfRooms(value: any) {
    return typeof value === 'number' && value > 0;
  }
}
