import { ClinicPreferenceName } from '../constants/ClinicPreferenceName';
import { Clinic } from './Clinic';

describe('ClinicEntity tests', () => {
  it('should create a valid entity', () => {
    const email = 'test@mail.com';
    const clinic = new Clinic([{ key: ClinicPreferenceName.CLINIC_EMAIL, value: email }]);

    expect(clinic.getPreference(ClinicPreferenceName.CLINIC_EMAIL)).toEqual(email);
    expect(clinic.getPreference(ClinicPreferenceName.CLINIC_NAME)).toEqual('<no name>');
  });

  it('should not create a valid entity', () => {
    expect(() => {
      const email = '';
      new Clinic([{ key: ClinicPreferenceName.CLINIC_EMAIL, value: email }]);
    }).toThrow();
  });
});
