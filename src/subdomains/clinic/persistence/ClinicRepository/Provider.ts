import { Clinic, PreferenceObject, RawPreferenceObject } from '../../entities/Clinic';
import { readFile, writeFile } from 'fs/promises';
import { ClassProvider, Injectable } from '@nestjs/common';
import { EventsService } from 'src/events/events.service';
import { ClinicPreferenceName } from '../../constants/ClinicPreferenceName';
import { IClinicRepository } from './IClinicRepository';
import { PersistenceType } from 'src/shared/types/PersistenceType';

const PreferencesFilename = './clinic.json';

@Injectable()
class ClinicRepository implements IClinicRepository {
  constructor(private readonly eventsService: EventsService) {}

  public async isValidVersion(): Promise<PersistenceType> {
    return PersistenceType.VALID;
  }

  public async loadClinic(): Promise<Clinic> {
    try {
      const json = await readFile(PreferencesFilename, { encoding: 'utf-8' });
      const parsedArray: PreferenceObject[] = (JSON.parse(json) as RawPreferenceObject[]).map(
        (p) => {
          const key = Object.keys(p)[0] as ClinicPreferenceName;
          const value = p[key];
          return { key, value };
        },
      );
      return new Clinic(parsedArray);
    } catch (e) {
      return new Clinic([]);
    }
  }

  public async persistClinic(clinic: Clinic): Promise<void> {
    try {
      await writeFile(PreferencesFilename, JSON.stringify(clinic.getPreferencesAsObject()), {
        encoding: 'utf-8',
      });
      await this.eventsService.emitAll(clinic.domainEvents);
    } catch (e) {
      throw new Error('Failed to write the preferences.');
    }
  }
}

export const ClinicRepositoryProvider: ClassProvider = {
  provide: IClinicRepository,
  useClass: ClinicRepository,
};
