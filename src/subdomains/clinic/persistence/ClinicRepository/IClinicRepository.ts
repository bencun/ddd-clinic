import { IRepository } from 'src/shared/ddd/IRepository';
import { PersistenceType } from 'src/shared/types/PersistenceType';
import { Clinic } from '../../entities/Clinic';

export abstract class IClinicRepository implements IRepository<Clinic> {
  abstract isValidVersion(entity: Clinic): Promise<PersistenceType>;
  abstract loadClinic(): Promise<Clinic>;
  abstract persistClinic(clinic: Clinic): Promise<void>;
}
