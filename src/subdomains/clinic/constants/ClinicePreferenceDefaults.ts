import { ClinicPreferenceName } from './ClinicPreferenceName';

export const ClinicPreferenceDefaults: { [k in ClinicPreferenceName]: any } = {
  [ClinicPreferenceName.CLINIC_NAME]: '<no name>',
  [ClinicPreferenceName.CLINIC_ADDRESS]: '<no address>',
  [ClinicPreferenceName.CLINIC_PHONE_NUMBER]: '<no phone>',
  [ClinicPreferenceName.CLINIC_EMAIL]: '<no email>',
  [ClinicPreferenceName.CLINIC_WEBSITE]: '<no website>',
  [ClinicPreferenceName.CLINIC_NUMBER_OF_ROOMS]: 2,
} as const;
