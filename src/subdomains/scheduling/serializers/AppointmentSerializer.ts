import { Injectable } from '@nestjs/common';
import { formatISO } from 'date-fns';
import { IEntitySerializer } from 'src/shared/ddd/IEntitySerializer';
import { Appointment } from '../entities/Appointment';
import { AppointmentResponseDTO } from './types/AppointmentResponseDTO';

@Injectable()
export class AppointmentSerializer
  implements IEntitySerializer<Appointment, AppointmentResponseDTO>
{
  serialize(a: Appointment): AppointmentResponseDTO {
    return {
      uuid: a.uuid,
      patientUuid: a.patientUuid,
      patientName: a.patientName,
      duration: a.duration,
      start: formatISO(a.AppointmentStart),
      status: a.Status,
    };
  }
}
