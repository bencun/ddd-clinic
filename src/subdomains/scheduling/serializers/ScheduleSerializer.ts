import { Injectable } from '@nestjs/common';
import { formatISO } from 'date-fns';
import { IEntitySerializer } from 'src/shared/ddd/IEntitySerializer';
import { Schedule } from '../entities/Schedule';
import { AppointmentSerializer } from './AppointmentSerializer';
import { ScheduleResponseDTO } from './types/ScheduleResponseDTO';

@Injectable()
export class ScheduleSerializer implements IEntitySerializer<Schedule, ScheduleResponseDTO> {
  constructor(private readonly appointmentSerializer: AppointmentSerializer) {}
  serialize(s: Schedule): ScheduleResponseDTO {
    return {
      date: formatISO(s.date),
      roomsAvailable: s.roomsAvailable,
      appointments: s.appointments.map((a) => this.appointmentSerializer.serialize(a)),
    };
  }
}
