import { AppointmentResponseDTO } from './AppointmentResponseDTO';

export type ScheduleResponseDTO = {
  date: string;
  roomsAvailable: number;
  appointments: AppointmentResponseDTO[];
};
