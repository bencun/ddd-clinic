import { Uuid } from 'src/shared/types/Uuid';
import { AppointmentStatus } from '../../constants/AppointmentStatus';

export type AppointmentResponseDTO = {
  uuid: Uuid;
  patientUuid: Uuid;
  patientName: string;
  start: string;
  duration: number;
  status: AppointmentStatus;
};
