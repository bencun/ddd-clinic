import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { DomainEvent } from 'src/shared/ddd/DomainEvent';
import { Uuid } from 'src/shared/types/Uuid';
import { Appointment } from '../entities/Appointment';

export class AppointmentStartedEvent extends DomainEvent {
  constructor(public readonly appointment: Appointment, public readonly practitionerUuid: Uuid) {
    super(true);
  }
  public get token(): DomainEventToken {
    return DomainEventToken.APPOINTMENT_STARTED;
  }
}
