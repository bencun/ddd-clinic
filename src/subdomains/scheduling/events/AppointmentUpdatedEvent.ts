import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { DomainEvent } from 'src/shared/ddd/DomainEvent';
import { Uuid } from 'src/shared/types/Uuid';

export class AppointmentUpdatedEvent extends DomainEvent {
  constructor(public readonly appointmentUuid: Uuid) {
    super(false);
  }

  public get token(): DomainEventToken {
    return DomainEventToken.APPOINTMENT_UPDATED;
  }
}
