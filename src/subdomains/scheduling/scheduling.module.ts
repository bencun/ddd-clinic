import { Module } from '@nestjs/common';
import { ClinicModule } from '../clinic/clinic.module';
import { ExaminationCanceledHandler } from './handlers/ExaminationCanceledHandler';
import { ExaminationCompletedHandler } from './handlers/ExaminationCompletedHandler';
import { SchedulingController } from './http/SchedulingController';
import { ScheduleRepositoryProvider } from './persistence/ScheduleRepository/Provider';
import { AppointmentSerializer } from './serializers/AppointmentSerializer';
import { ScheduleSerializer } from './serializers/ScheduleSerializer';
import { CancelAppointment } from './useCases/CancelAppointment';
import { CreateAppointment } from './useCases/CreateAppointment';
import { FinishAppointment } from './useCases/FinishAppointment';
import { GetSchedule } from './useCases/GetSchedule';
import { RescheduleAppointment } from './useCases/RescheduleAppointment';
import { StartAppointment } from './useCases/StartAppointment';

@Module({
  controllers: [SchedulingController],
  providers: [
    ScheduleRepositoryProvider,
    GetSchedule,
    CreateAppointment,
    StartAppointment,
    CancelAppointment,
    FinishAppointment,
    RescheduleAppointment,
    ExaminationCompletedHandler,
    ExaminationCanceledHandler,
    ScheduleSerializer,
    AppointmentSerializer,
  ],
  imports: [ClinicModule],
})
export class SchedulingModule {}
