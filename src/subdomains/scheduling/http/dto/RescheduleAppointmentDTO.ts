import { IsNumber, IsOptional, Max, Min } from 'class-validator';
import { IsValidTimestamp } from 'src/shared/decorators/IsValidTimestamp';

export class RescheduleAppointmentDTO {
  @IsOptional()
  @IsValidTimestamp()
  start?: Date;

  @IsOptional()
  @IsNumber()
  @Min(5)
  @Max(60)
  duration?: number;
}
