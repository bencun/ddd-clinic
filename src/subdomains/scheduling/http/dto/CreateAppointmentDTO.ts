import { IsNumber, IsString, IsUUID, Max, Min, MinLength } from 'class-validator';
import { IsValidTimestamp } from 'src/shared/decorators/IsValidTimestamp';
import { Uuid } from 'src/shared/types/Uuid';

export class CreateAppointmentDTO {
  @IsUUID('4')
  patientUuid: Uuid;

  @IsString()
  @MinLength(2)
  patientName: string;

  @IsValidTimestamp()
  start: Date;

  @IsNumber()
  @Min(5)
  @Max(60)
  duration: number;
}
