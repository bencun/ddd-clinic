import { IsValidTimestamp } from 'src/shared/decorators/IsValidTimestamp';
import { UuidDTO } from 'src/shared/dtos/UuidDTO';

export class FinishAppointmentDTO extends UuidDTO {
  @IsValidTimestamp()
  date: Date;
}
