import { Role } from '.prisma/client';
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Req,
} from '@nestjs/common';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { DateDTO } from 'src/shared/dtos/DateDTO';
import { ClinicPreferenceName } from 'src/subdomains/clinic/constants/ClinicPreferenceName';
import { GetClinic } from 'src/subdomains/clinic/useCases/GetClinic';
import { CreateAppointmentDTO } from './dto/CreateAppointmentDTO';
import { CancelAppointment } from '../useCases/CancelAppointment';
import { CreateAppointment } from '../useCases/CreateAppointment';
import { GetSchedule } from '../useCases/GetSchedule';
import { StartAppointment } from '../useCases/StartAppointment';
import RequestWithUser from 'src/auth/interfaces/request-with-user.interface';
import { UuidDTO } from 'src/shared/dtos/UuidDTO';
import { ScheduleSerializer } from '../serializers/ScheduleSerializer';
import { RescheduleAppointment } from '../useCases/RescheduleAppointment';
import { RescheduleAppointmentDTO } from './dto/RescheduleAppointmentDTO';

@Controller('schedule')
export class SchedulingController {
  constructor(
    private readonly getSchedule: GetSchedule,
    private readonly createAppointment: CreateAppointment,
    private readonly startAppointment: StartAppointment,
    private readonly cancelAppointment: CancelAppointment,
    private readonly rescheduleAppointment: RescheduleAppointment,
    private readonly getClinic: GetClinic,
    private readonly scheduleSerializer: ScheduleSerializer,
  ) {}

  private async getNumberOfRooms(): Promise<number> {
    return (await this.getClinic.execute()).getPreference(
      ClinicPreferenceName.CLINIC_NUMBER_OF_ROOMS,
    );
  }

  @Roles(Role.NURSE, Role.PRACTITIONER)
  @Get(':date')
  async getScheduleController(@Param() { date }: DateDTO) {
    const schedule = await this.getSchedule.execute(date, await this.getNumberOfRooms());
    return this.scheduleSerializer.serialize(schedule);
  }

  @Roles(Role.NURSE)
  @Post()
  async createAppointmentController(@Body() appointmentDto: CreateAppointmentDTO) {
    try {
      return await this.createAppointment.execute(appointmentDto, await this.getNumberOfRooms());
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @Roles(Role.NURSE)
  @Patch('rescheduleAppointment/:uuid')
  async rescheduleAppointmentController(
    @Req() req: RequestWithUser,
    @Param() { uuid }: UuidDTO,
    @Body() { duration, start }: RescheduleAppointmentDTO,
  ) {
    if (!duration && !start)
      throw new BadRequestException('Neither the new date nor appointment duration were provided.');

    try {
      await this.rescheduleAppointment.execute(
        uuid,
        await this.getNumberOfRooms(),
        start,
        duration,
      );
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @Roles(Role.PRACTITIONER)
  @Patch('startAppointment/:uuid')
  async startAppointmentController(@Req() req: RequestWithUser, @Param() { uuid }: UuidDTO) {
    try {
      await this.startAppointment.execute(req.user.uuid, uuid, await this.getNumberOfRooms());
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @Roles(Role.NURSE)
  @Patch('cancelAppointment/:uuid')
  async cancelAppointmentController(@Param() { uuid }: UuidDTO) {
    try {
      await this.cancelAppointment.execute(uuid, await this.getNumberOfRooms());
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }
}
