import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { IEventHandler } from 'src/shared/ddd/IEventHandler';
import { ClinicPreferenceName } from 'src/subdomains/clinic/constants/ClinicPreferenceName';
import { GetClinic } from 'src/subdomains/clinic/useCases/GetClinic';
import { ExaminationCompletedEvent } from 'src/subdomains/examinations/events/ExaminationCompletedEvent';
import { FinishAppointment } from '../useCases/FinishAppointment';

@Injectable()
export class ExaminationCompletedHandler implements IEventHandler<ExaminationCompletedEvent> {
  constructor(
    private readonly finishAppointment: FinishAppointment,
    private readonly getClinic: GetClinic,
  ) {}

  @OnEvent(DomainEventToken.EXAMINATION_COMPLETED)
  async handle(event: ExaminationCompletedEvent): Promise<any> {
    const roomsAvailable = (await this.getClinic.execute()).getPreference(
      ClinicPreferenceName.CLINIC_NUMBER_OF_ROOMS,
    ) as number;
    await this.finishAppointment.execute(event.examination.appointmentUuid, roomsAvailable);
    // TODO notify using websockets
  }
}
