import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { IEventHandler } from 'src/shared/ddd/IEventHandler';
import { ClinicPreferenceName } from 'src/subdomains/clinic/constants/ClinicPreferenceName';
import { GetClinic } from 'src/subdomains/clinic/useCases/GetClinic';
import { ExaminationCanceledEvent } from 'src/subdomains/examinations/events/ExaminationCanceledEvent';
import { FinishAppointment } from '../useCases/FinishAppointment';

@Injectable()
export class ExaminationCanceledHandler implements IEventHandler<ExaminationCanceledEvent> {
  constructor(
    private readonly finishAppointment: FinishAppointment,
    private readonly getClinic: GetClinic,
  ) {}

  @OnEvent(DomainEventToken.EXAMINATION_CANCELED)
  async handle(event: ExaminationCanceledEvent): Promise<any> {
    const roomsAvailable = (await this.getClinic.execute()).getPreference(
      ClinicPreferenceName.CLINIC_NUMBER_OF_ROOMS,
    ) as number;
    await this.finishAppointment.execute(event.examination.appointmentUuid, roomsAvailable);
    // TODO notify using websockets
  }
}
