import { Injectable } from '@nestjs/common';
import { startOfWeek } from 'date-fns';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { CreateAppointmentDTO } from '../http/dto/CreateAppointmentDTO';
import { IScheduleRepository } from '../persistence/ScheduleRepository/IScheduleRepository';
import { Appointment } from '../entities/Appointment';
import { UuidDTO } from 'src/shared/dtos/UuidDTO';

@Injectable()
export class CreateAppointment implements IUseCase<UuidDTO> {
  constructor(private readonly scheduleRepo: IScheduleRepository) {}

  async execute(appointmentDto: CreateAppointmentDTO, roomsAvailable: number): Promise<UuidDTO> {
    const schedule = await this.scheduleRepo.loadSchedule(
      startOfWeek(appointmentDto.start, { weekStartsOn: 1 }),
      roomsAvailable,
    );
    const appointment = new Appointment(
      appointmentDto.patientUuid,
      appointmentDto.patientName,
      appointmentDto.start,
      appointmentDto.duration,
      1,
    );
    schedule.createAppointment(appointment);
    await this.scheduleRepo.saveSchedule(schedule);
    return { uuid: appointment.uuid };
  }
}
