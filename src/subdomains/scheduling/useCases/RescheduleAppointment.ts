import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Uuid } from 'src/shared/types/Uuid';
import { IScheduleRepository } from '../persistence/ScheduleRepository/IScheduleRepository';

@Injectable()
export class RescheduleAppointment implements IUseCase<void> {
  constructor(private readonly scheduleRepo: IScheduleRepository) {}

  async execute(
    uuid: Uuid,
    roomsAvailable: number,
    newDate?: Date,
    newDuration?: number,
  ): Promise<void> {
    const schedule = await this.scheduleRepo.loadScheduleForAppointment(uuid, roomsAvailable);
    schedule.rescheduleAppointment(uuid, newDate, newDuration);
    await this.scheduleRepo.saveSchedule(schedule);
  }
}
