import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Schedule } from '../entities/Schedule';
import { IScheduleRepository } from '../persistence/ScheduleRepository/IScheduleRepository';
import { Uuid } from 'src/shared/types/Uuid';

@Injectable()
export class FinishAppointment implements IUseCase<Schedule> {
  constructor(private readonly scheduleRepo: IScheduleRepository) {}

  async execute(uuid: Uuid, roomsAvailable: number): Promise<Schedule> {
    const schedule = await this.scheduleRepo.loadScheduleForAppointment(uuid, roomsAvailable);
    schedule.finishAppointment(uuid);
    await this.scheduleRepo.saveSchedule(schedule);
    return schedule;
  }
}
