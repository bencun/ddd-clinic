import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { IScheduleRepository } from '../persistence/ScheduleRepository/IScheduleRepository';
import { Uuid } from 'src/shared/types/Uuid';
import { UuidDTO } from 'src/shared/dtos/UuidDTO';

@Injectable()
export class StartAppointment implements IUseCase<UuidDTO> {
  constructor(private readonly scheduleRepo: IScheduleRepository) {}

  async execute(
    practitionerUuid: Uuid,
    appointmentUuid: Uuid,
    roomsAvailable: number,
  ): Promise<UuidDTO> {
    const schedule = await this.scheduleRepo.loadScheduleForAppointment(
      appointmentUuid,
      roomsAvailable,
    );
    schedule.startAppointment(appointmentUuid, practitionerUuid);
    await this.scheduleRepo.saveSchedule(schedule);
    return { uuid: schedule.uuid };
  }
}
