import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Schedule } from '../entities/Schedule';
import { IScheduleRepository } from '../persistence/ScheduleRepository/IScheduleRepository';

@Injectable()
export class GetSchedule implements IUseCase<Schedule> {
  constructor(private readonly scheduleRepo: IScheduleRepository) {}

  async execute(date: Date, roomsAvailable: number): Promise<Schedule> {
    return await this.scheduleRepo.loadSchedule(date, roomsAvailable);
  }
}
