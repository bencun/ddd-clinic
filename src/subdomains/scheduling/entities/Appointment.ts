import { BaseEntity } from 'src/shared/ddd/BaseEntity';
import { Uuid } from 'src/shared/types/Uuid';
import { AppointmentStatus } from '../constants/AppointmentStatus';
import { addMinutes } from 'date-fns';

export class Appointment extends BaseEntity {
  constructor(
    public readonly patientUuid: Uuid,
    public patientName: string,
    public start: Date,
    public duration: number,
    version: number,
    private _status: AppointmentStatus = AppointmentStatus.NONE,
    uuid?: Uuid,
  ) {
    super(version, uuid);
  }

  get Status() {
    return this._status;
  }

  get AppointmentStart() {
    return this.start;
  }

  get AppointmentEnd() {
    return addMinutes(this.start, this.duration);
  }

  public StartAppointment() {
    if (this._status === AppointmentStatus.NONE) {
      this._status = AppointmentStatus.STARTED;
    } else throw new Error('Appointment cannot be started.');
  }

  public CancelAppointment() {
    if (this._status === AppointmentStatus.NONE) {
      this._status = AppointmentStatus.CANCELED;
    } else throw new Error('Appointment cannot be cancelled.');
  }

  public FinishAppointment() {
    if (this._status === AppointmentStatus.STARTED) {
      this._status = AppointmentStatus.FINISHED;
    } else throw new Error('Appointment cannot be finished since it has not been started.');
  }
}
