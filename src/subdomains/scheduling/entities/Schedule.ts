import { AggregateRoot } from 'src/shared/ddd/AggregateRoot';
import { Uuid } from 'src/shared/types/Uuid';
import { Appointment } from './Appointment';
import {
  getDay,
  areIntervalsOverlapping,
  Interval,
  getHours,
  isAfter,
  startOfWeek,
  formatISO,
} from 'date-fns';
import { AppointmentStatus } from '../constants/AppointmentStatus';
import { AppointmentCreatedEvent } from '../events/AppointmentCreatedEvent';
import { AppointmentCanceledEvent } from '../events/AppointmentCanceledEvent';
import { addMinutes } from 'date-fns';
import { AppointmentUpdatedEvent } from '../events/AppointmentUpdatedEvent';
import { AppointmentStartedEvent } from '../events/AppointmentStartedEvent';
import { AppointmentFinishedEvent } from '../events/AppointmentFinishedEvent';

/**
 * Non-persistent model - no direct mapping to the database here!
 * Pure domain logic related domain object!
 */
export class Schedule extends AggregateRoot {
  constructor(
    public readonly date: Date,
    public readonly appointments: Appointment[],
    public readonly roomsAvailable: number,
  ) {
    super(1, formatISO(startOfWeek(date, { weekStartsOn: 1 })));
    if (getDay(date) !== 1) {
      throw new Error('A schedule cannot start on a day other than Monday!');
    }
  }

  private findAppointment(appointmentUuid: Uuid) {
    return this.appointments.find((a) => a.uuid === appointmentUuid);
  }

  private findOverlappingAppointments(
    interval: Interval,
    includeCanceled = false,
    includeFinished = false,
  ): ReadonlyArray<Appointment> {
    return this.appointments.filter(
      (a) =>
        areIntervalsOverlapping({ start: a.AppointmentStart, end: a.AppointmentEnd }, interval) &&
        (includeCanceled ? true : a.Status !== AppointmentStatus.CANCELED) &&
        (includeFinished ? true : a.Status !== AppointmentStatus.FINISHED),
    );
  }

  private intervalFitsWorkingHours(start: Date, duration: number) {
    const startWorkingHours = 8;
    const endWorkingHours = 20;
    return (
      getHours(start) >= startWorkingHours &&
      getHours(addMinutes(start, duration)) <= endWorkingHours
    );
  }

  public createAppointment(newAppointment: Appointment) {
    if (newAppointment.duration < 5 || newAppointment.duration > 60) {
      throw new Error('Appointment duration must be between 5 minutes and 60 minutes.');
    }
    if (getDay(newAppointment.start) === 0) {
      throw new Error('Appointment cannot be scheduled on Sunday.');
    }
    if (!isAfter(newAppointment.start, new Date())) {
      throw new Error('The date is in the past!');
    }
    // 1. find any potentially overlapping appointments first
    const overlappingAppointments = this.findOverlappingAppointments({
      start: newAppointment.AppointmentStart,
      end: newAppointment.AppointmentEnd,
    });
    if (overlappingAppointments.length > 0) {
      // 2. then check if any those are for the same patient - if yes then reject since patient can't be in two places at the same time
      if (overlappingAppointments.find((a) => a.patientUuid === newAppointment.patientUuid)) {
        throw new Error('Patient already has an appointment scheduled at this time.');
      }
      // 3. check how many overlapping appointments are there, if < roomsAvailable then schedule the appointment
      if (overlappingAppointments.length >= this.roomsAvailable) {
        throw new Error('Not enough rooms available to schedule this appointment.');
      }
      // 4. check if it fits the working hours
      if (!this.intervalFitsWorkingHours(newAppointment.start, newAppointment.duration)) {
        throw new Error('Appointment is outside of working hours.');
      }
    }
    // proceed to schedule the appointment
    this.appointments.push(newAppointment);
    // publish an event
    this.addEvent(new AppointmentCreatedEvent(newAppointment.uuid));
  }

  public rescheduleAppointment(appointmentUuid: Uuid, _datetime?: Date, _duration?: number) {
    const appointmentToReschedule = this.findAppointment(appointmentUuid);
    if (appointmentToReschedule) {
      if (appointmentToReschedule.Status !== AppointmentStatus.NONE) {
        throw new Error(
          'You cannot reschedule an appointment that is in progress or was cancelled or has already finished.',
        );
      }
      const newDuration = _duration || appointmentToReschedule.duration;
      const newDatetime = _datetime || appointmentToReschedule.AppointmentStart;
      if (newDuration < 5 || newDuration > 60) {
        throw new Error('Appointment duration must be between 5 minutes and 60 minutes.');
      }
      if (getDay(newDatetime) === 0) {
        throw new Error('Appointment cannot be scheduled on Sunday.');
      }
      // 0. check if date is not in the past
      if (!isAfter(newDatetime, new Date())) {
        throw new Error('The date is in the past!');
      }
      // 1. check if any appointments are overlapping with the new date and time
      const overlappingAppointments = this.findOverlappingAppointments({
        start: newDatetime,
        end: addMinutes(newDatetime, newDuration),
      })
        //...but exclude the appointment we're rescheduling
        .filter((a) => a.uuid !== appointmentUuid);
      if (overlappingAppointments.length > 0) {
        // 2. then check if any of those are for the same patient - if yes then reject since patient can't be in two places at the same time
        if (
          overlappingAppointments.find((a) => a.patientUuid === appointmentToReschedule.patientUuid)
        ) {
          throw new Error('Patient already has an appointment scheduled at this time.');
        }
        // 3. check how many overlapping appointments are there, if < roomsAvailable then schedule the appointment
        if (overlappingAppointments.length >= this.roomsAvailable) {
          throw new Error('Not enough rooms available to schedule this appointment');
        }
        // 4. check if it fits the working hours
        if (!this.intervalFitsWorkingHours(newDatetime, newDuration)) {
          throw new Error('Appointment is outside of working hours.');
        }
      }
      // reschedule
      appointmentToReschedule.start = newDatetime;
      appointmentToReschedule.duration = newDuration;
      // publish the event
      this.addEvent(new AppointmentUpdatedEvent(appointmentToReschedule.uuid));
    } else {
      throw new Error('Appointment not found!');
    }
  }

  public cancelAppointment(appointmentUuid: Uuid) {
    const appointment = this.findAppointment(appointmentUuid);
    if (appointment) {
      appointment.CancelAppointment();
      this.addEvent(new AppointmentCanceledEvent(appointment.uuid));
    } else {
      throw new Error('Appointment not found!');
    }
  }

  public startAppointment(appointmentUuid: Uuid, practitionerUuid: Uuid) {
    const appointment = this.findAppointment(appointmentUuid);
    if (appointment) {
      appointment.StartAppointment();
      this.addEvent(new AppointmentStartedEvent(appointment, practitionerUuid));
    } else {
      throw new Error('Appointment not found!');
    }
  }

  public finishAppointment(appointmentUuid: Uuid) {
    const appointment = this.findAppointment(appointmentUuid);
    if (appointment) {
      appointment.FinishAppointment();
      this.addEvent(new AppointmentFinishedEvent(appointment.uuid));
    } else {
      throw new Error('Appointment not found!');
    }
  }
}
