export enum AppointmentStatus {
  NONE = 'NONE',
  STARTED = 'STARTED',
  FINISHED = 'FINISHED',
  CANCELED = 'CANCELED',
}
