import { IRepository } from 'src/shared/ddd/IRepository';
import { PersistenceType } from 'src/shared/types/PersistenceType';
import { Uuid } from 'src/shared/types/Uuid';
import { Schedule } from '../../entities/Schedule';

export abstract class IScheduleRepository implements IRepository<Schedule> {
  abstract isValidVersion(entity: Schedule): Promise<PersistenceType>;
  abstract loadSchedule(date: Date, roomsAvailable: number): Promise<Schedule>;
  abstract loadScheduleForAppointment(
    apppointmentUuid: Uuid,
    roomsAvailable: number,
  ): Promise<Schedule>;
  abstract saveSchedule(schedule: Schedule): Promise<void>;
}
