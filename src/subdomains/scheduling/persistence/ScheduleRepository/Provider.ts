import { BadRequestException, ClassProvider, Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/database/prisma.service';
import { PersistenceType } from 'src/shared/types/PersistenceType';
import { Schedule } from '../../entities/Schedule';
import { IScheduleRepository } from './IScheduleRepository';
import { endOfWeek, getDay, startOfWeek } from 'date-fns';
import { Appointment } from '../../entities/Appointment';
import { AppointmentStatus } from '../../constants/AppointmentStatus';
import { EventsService } from 'src/events/events.service';
import { Uuid } from 'src/shared/types/Uuid';

@Injectable()
class ScheduleRepository implements IScheduleRepository {
  constructor(
    private readonly prisma: PrismaService,
    private readonly eventsService: EventsService,
  ) {}

  private async loadAppointmentsForWeek(weekStartDate: Date): Promise<Appointment[]> {
    const day = getDay(weekStartDate);
    if (day !== 1) throw new BadRequestException('The day is not Monday.');
    const startOfWeekDate = startOfWeek(weekStartDate, { weekStartsOn: 1 });
    const endOfWeekDate = endOfWeek(weekStartDate, { weekStartsOn: 1 });
    const appointments = await this.prisma.appointment.findMany({
      where: {
        start: { gte: startOfWeekDate, lte: endOfWeekDate },
      },
    });

    return appointments.map(
      (a) =>
        new Appointment(
          a.patientUuid,
          a.patientName,
          a.start,
          a.duration,
          a.version,
          a.status as AppointmentStatus,
          a.uuid,
        ),
    );
  }

  private async saveAppointments(schedule: Schedule): Promise<void> {
    const prismaPromises = [];
    for (const appointment of schedule.appointments) {
      // save existing and new appointments
      const query = this.prisma.appointment.upsert({
        where: { uuid: appointment.uuid },
        create: {
          uuid: appointment.uuid,
          start: appointment.start,
          duration: appointment.duration,
          patientName: appointment.patientName,
          patientUuid: appointment.patientUuid,
          status: appointment.Status,
          version: appointment.version,
        },
        update: {
          start: appointment.start,
          duration: appointment.duration,
          patientName: appointment.patientName,
          patientUuid: appointment.patientUuid,
          status: appointment.Status,
          version: { increment: 1 },
        },
      });
      prismaPromises.push(query);
    }
    // perform the transaction
    await this.prisma.$transaction(prismaPromises);
  }

  async isValidVersion(entity: Schedule): Promise<PersistenceType> {
    // check all appointments in the db
    const existingAppointments = await this.prisma.appointment.findMany({
      where: {
        uuid: { in: entity.appointments.map((a) => a.uuid) },
      },
    });
    // check if we're trying to store an invalid version of existing appointments
    for (const dbAppointment of existingAppointments) {
      const entityAppointment = entity.appointments.find((a) => a.uuid === dbAppointment.uuid);
      if (dbAppointment.version !== entityAppointment.version) {
        return PersistenceType.INVALID;
      }
    }
    return PersistenceType.VALID;
  }

  async loadSchedule(date: Date, roomsAvailable: number): Promise<Schedule> {
    const appointments = await this.loadAppointmentsForWeek(date);
    return new Schedule(date, appointments, roomsAvailable);
  }

  async loadScheduleForAppointment(
    apppointmentUuid: Uuid,
    roomsAvailable: number,
  ): Promise<Schedule> {
    const appointment = await this.prisma.appointment.findUnique({
      where: { uuid: apppointmentUuid },
    });
    if (!appointment) throw new NotFoundException();
    return await this.loadSchedule(
      startOfWeek(appointment.start, { weekStartsOn: 1 }),
      roomsAvailable,
    );
  }

  async saveSchedule(schedule: Schedule): Promise<void> {
    const validity = await this.isValidVersion(schedule);
    if (validity === PersistenceType.VALID) {
      await this.saveAppointments(schedule);
      // trigger the events
      this.eventsService.emitAll(schedule.domainEvents);
    } else {
      throw new Error('Schedule version mismatch!');
    }
  }
}

export const ScheduleRepositoryProvider: ClassProvider = {
  provide: IScheduleRepository,
  useClass: ScheduleRepository,
};
