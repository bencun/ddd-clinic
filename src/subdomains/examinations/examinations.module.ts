import { Module } from '@nestjs/common';
import { PatientsModule } from '../patients/patients.module';
import { AppointmentStartedEventHandler } from './handlers/AppointmentStartedEventHandler';
import { ExaminationsController } from './http/ExaminationsController';
import { ExaminationRepositoryProvider } from './persistence/ExaminationRepository/Provider';
import { ExaminationSerializer } from './serializers/ExaminationSerializer';
import { ConcludeExamination } from './useCases/ConcludeExamination';
import { GetActiveExaminations } from './useCases/GetActiveExaminations';
import { GetExamination } from './useCases/GetExamination';
import { StartExamination } from './useCases/StartExamination';

@Module({
  controllers: [ExaminationsController],
  providers: [
    ExaminationRepositoryProvider,
    StartExamination,
    GetExamination,
    ConcludeExamination,
    AppointmentStartedEventHandler,
    GetActiveExaminations,
    ExaminationSerializer,
  ],
  exports: [StartExamination],
  imports: [PatientsModule],
})
export class ExaminationModule {}
