import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Uuid } from 'src/shared/types/Uuid';
import { Examination } from '../entities/Examination';
import { IExaminationRepository } from '../persistence/ExaminationRepository/IExaminationRepository';

@Injectable()
export class GetExamination implements IUseCase<Examination> {
  constructor(private readonly examinationRepo: IExaminationRepository) {}

  async execute(uuid: Uuid): Promise<Examination> {
    const e = await this.examinationRepo.loadExamination(uuid);
    return e;
  }
}
