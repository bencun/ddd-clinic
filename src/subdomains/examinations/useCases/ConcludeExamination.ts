import { ForbiddenException, Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Uuid } from 'src/shared/types/Uuid';
import { Diagnosis } from 'src/shared/valueObjects/Diagnosis';
import { PrescriptionsMapper } from 'src/shared/valueObjects/mappers/PrescriptionsMapper';
import { ExaminationDataDTO } from '../http/dto/ExaminationDataDTO';
import { IExaminationRepository } from '../persistence/ExaminationRepository/IExaminationRepository';
import { ExaminationData } from '../valueObjects/ExaminationData';

@Injectable()
export class ConcludeExamination implements IUseCase<void> {
  constructor(private readonly examinationRepo: IExaminationRepository) {}

  async execute(
    completionType: 'cancel' | 'complete',
    practitionerUuid: Uuid,
    uuid: Uuid,
    edDto: ExaminationDataDTO,
  ): Promise<void> {
    const examination = await this.examinationRepo.loadExamination(uuid);
    if (examination.practitionerUuid !== practitionerUuid) {
      throw new ForbiddenException();
    }
    const examinationData = new ExaminationData(
      edDto.anamnesis,
      edDto.diagnoses.map((d) => new Diagnosis(d)),
      edDto.prescriptions.map((p) => PrescriptionsMapper.toValueObject(p)),
      edDto.chronicDiagnosesEstablished.map((d) => new Diagnosis(d)),
      edDto.chronicDiagnosesRemoved.map((d) => new Diagnosis(d)),
    );
    if (completionType === 'cancel') {
      examination.cancelExamination(examinationData);
    } else if (completionType === 'complete') {
      examination.completeExamination(examinationData);
    }
    await this.examinationRepo.saveExamination(examination);
  }
}
