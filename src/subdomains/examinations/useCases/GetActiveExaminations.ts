import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { ExaminationStatus } from '../constants/ExaminationStatus';
import { Examination } from '../entities/Examination';
import { IExaminationRepository } from '../persistence/ExaminationRepository/IExaminationRepository';

@Injectable()
export class GetActiveExaminations implements IUseCase<Examination[]> {
  constructor(private readonly examinationRepo: IExaminationRepository) {}

  async execute(practitionerUuid): Promise<Examination[]> {
    return (await this.examinationRepo.loadExaminationsForPractitioner(practitionerUuid)).filter(
      (e) => e.Status === ExaminationStatus.IN_PROGRESS,
    );
  }
}
