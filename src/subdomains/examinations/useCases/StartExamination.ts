import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Uuid } from 'src/shared/types/Uuid';
import { Diagnosis } from 'src/shared/valueObjects/Diagnosis';
import { Examination } from '../entities/Examination';
import { IExaminationRepository } from '../persistence/ExaminationRepository/IExaminationRepository';

@Injectable()
export class StartExamination implements IUseCase<Examination> {
  constructor(private readonly examinationRepo: IExaminationRepository) {}

  async execute(
    appointmentUuid: Uuid,
    practitionerUuid: Uuid,
    patientUuid: Uuid,
    patientName: string,
    knownChronicDiagnoses: Diagnosis[],
  ): Promise<Examination> {
    const examination = new Examination(
      appointmentUuid,
      practitionerUuid,
      patientUuid,
      patientName,
      knownChronicDiagnoses,
    );
    await this.examinationRepo.saveExamination(examination);
    return examination;
  }
}
