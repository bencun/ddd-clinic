import { AggregateRoot } from 'src/shared/ddd/AggregateRoot';
import { Uuid } from 'src/shared/types/Uuid';
import { Diagnosis } from 'src/shared/valueObjects/Diagnosis';
import { ExaminationStatus } from '../constants/ExaminationStatus';
import { ExaminationCanceledEvent } from '../events/ExaminationCanceledEvent';
import { ExaminationCompletedEvent } from '../events/ExaminationCompletedEvent';
import { ExaminationData } from '../valueObjects/ExaminationData';

export class Examination extends AggregateRoot {
  constructor(
    public readonly appointmentUuid: Uuid,
    public readonly practitionerUuid: Uuid,
    public readonly patientUuid: Uuid,
    public patientName: string,
    public knownChronicDiagnoses: Diagnosis[],
    public readonly startedAt: Date = new Date(),
    private _examinationData?: ExaminationData,
    private _status: ExaminationStatus = ExaminationStatus.IN_PROGRESS,
    private _endedAt?: Date,
    version = 1,
    uuid?: Uuid,
  ) {
    super(version, uuid);
  }

  get Status() {
    return this._status;
  }

  get endedAt() {
    return this._endedAt;
  }

  get ExaminationData() {
    return this._examinationData;
  }

  public completeExamination(examinationData: ExaminationData) {
    if (this._status === ExaminationStatus.IN_PROGRESS) {
      this._examinationData = examinationData;
      this._status = ExaminationStatus.COMPLETE;
      this._endedAt = new Date();
      this.addEvent(new ExaminationCompletedEvent(this));
    } else {
      throw new Error('Examination cannot be completed as it currently is not in progress!');
    }
  }

  public cancelExamination(examinationData: ExaminationData) {
    if (this._status === ExaminationStatus.IN_PROGRESS) {
      this._examinationData = examinationData;
      this._status = ExaminationStatus.CANCELED;
      this._endedAt = new Date();
      this.addEvent(new ExaminationCanceledEvent(this));
    } else {
      throw new Error('Examination cannot be canceled as it currently is not in progress!');
    }
  }
}
