import { ValueObject } from 'src/shared/ddd/ValueObject';
import { Diagnosis } from 'src/shared/valueObjects/Diagnosis';
import { Prescription } from 'src/shared/valueObjects/Prescription';

type TExaminationData = {
  anamnesis: string;
  diagnoses: Diagnosis[];
  prescriptions: Prescription[];
  chronicDiagnosesEstablished: Diagnosis[];
  chronicDiagnosesRemoved: Diagnosis[];
  notes?: string;
};

export class ExaminationData extends ValueObject<TExaminationData> {
  constructor(
    anamnesis: string,
    diagnoses: Diagnosis[],
    prescriptions: Prescription[],
    chronicDiagnosesEstablished: Diagnosis[],
    chronicDiagnosesRemoved: Diagnosis[],
    notes?: string,
  ) {
    const validAnamnesis = anamnesis.trim().length > 0;
    if (validAnamnesis) {
      super({
        anamnesis,
        diagnoses,
        chronicDiagnosesEstablished,
        chronicDiagnosesRemoved,
        prescriptions,
        notes,
      });
    } else {
      throw new Error('Examination data is invalid');
    }
  }
}
