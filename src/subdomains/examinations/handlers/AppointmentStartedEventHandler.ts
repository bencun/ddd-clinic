import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { IEventHandler } from 'src/shared/ddd/IEventHandler';
import { GetKnownChronicDiagnoses } from 'src/subdomains/patients/useCases/GetKnownChronicDiagnoses';
import { AppointmentStartedEvent } from 'src/subdomains/scheduling/events/AppointmentStartedEvent';
import { StartExamination } from '../useCases/StartExamination';

@Injectable()
export class AppointmentStartedEventHandler implements IEventHandler<AppointmentStartedEvent> {
  constructor(
    private readonly startExamination: StartExamination,
    private readonly getKnownChronicDiagnoses: GetKnownChronicDiagnoses,
  ) {}

  @OnEvent(DomainEventToken.APPOINTMENT_STARTED)
  async handle(event: AppointmentStartedEvent): Promise<any> {
    const examination = await this.startExamination.execute(
      event.appointment.uuid,
      event.practitionerUuid,
      event.appointment.patientUuid,
      event.appointment.patientName,
      await this.getKnownChronicDiagnoses.execute(event.appointment.patientUuid),
    );
    // TODO notify using websockets
  }
}
