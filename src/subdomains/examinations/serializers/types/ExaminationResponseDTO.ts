import { Uuid } from 'src/shared/types/Uuid';
import { RawPrescriptionsArray } from 'src/shared/valueObjects/mappers/PrescriptionsMapper';

export type ExaminationResponseDTO = {
  uuid: Uuid;
  appointmentUuid: Uuid;
  practitionerUuid: Uuid;
  patientUuid: Uuid;
  patientName: string;
  knownChronicDiagnoses: string[];
  startedAt: string;
  endedAt: string | null;
  status: string;
  examinationData?: {
    anamnesis: string;
    diagnoses: string[];
    chronicDiagnosesEstablished: string[];
    chronicDiagnosesRemoved: string[];
    prescriptions: RawPrescriptionsArray;
    notes: string;
  };
};
