import { Injectable } from '@nestjs/common';
import { formatISO } from 'date-fns';
import { IEntitySerializer } from 'src/shared/ddd/IEntitySerializer';
import { PrescriptionsMapper } from 'src/shared/valueObjects/mappers/PrescriptionsMapper';
import { Examination } from '../entities/Examination';
import { ExaminationResponseDTO } from './types/ExaminationResponseDTO';

@Injectable()
export class ExaminationSerializer
  implements IEntitySerializer<Examination, ExaminationResponseDTO>
{
  serialize(e: Examination): ExaminationResponseDTO {
    const eData = e.ExaminationData;

    return {
      uuid: e.uuid,
      appointmentUuid: e.appointmentUuid,
      practitionerUuid: e.practitionerUuid,
      patientUuid: e.patientUuid,
      patientName: e.patientName,
      startedAt: formatISO(e.startedAt),
      endedAt: e.endedAt ? formatISO(e.endedAt) : null,
      knownChronicDiagnoses: e.knownChronicDiagnoses.map((d) => d.props.code),
      status: e.Status,
      examinationData: eData
        ? {
            anamnesis: eData.props.anamnesis,
            diagnoses: eData.props.diagnoses.map((d) => d.props.code),
            chronicDiagnosesEstablished: eData.props.chronicDiagnosesEstablished.map(
              (d) => d.props.code,
            ),
            chronicDiagnosesRemoved: eData.props.chronicDiagnosesRemoved.map((d) => d.props.code),
            prescriptions: eData.props.prescriptions.map((p) =>
              PrescriptionsMapper.toPersistenceObject(p),
            ),
            notes: eData.props.notes,
          }
        : undefined,
    };
  }
}
