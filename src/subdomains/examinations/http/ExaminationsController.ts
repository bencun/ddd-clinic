import { Role } from '.prisma/client';
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Req,
} from '@nestjs/common';
import { Roles } from 'src/auth/decorators/roles.decorator';
import RequestWithUser from 'src/auth/interfaces/request-with-user.interface';
import { UuidDTO } from 'src/shared/dtos/UuidDTO';
import { ExaminationSerializer } from '../serializers/ExaminationSerializer';
import { ConcludeExamination } from '../useCases/ConcludeExamination';
import { GetActiveExaminations } from '../useCases/GetActiveExaminations';
import { GetExamination } from '../useCases/GetExamination';
import { ConcludeExaminationDTO } from './dto/ConcludeExaminationDTO';

@Controller('examinations')
export class ExaminationsController {
  constructor(
    private readonly getExamination: GetExamination,
    private readonly getActiveExaminations: GetActiveExaminations,
    private readonly concludeExamination: ConcludeExamination,
    private readonly examinationSerializer: ExaminationSerializer,
  ) {}

  @Roles(Role.PRACTITIONER)
  @Get('active')
  async getActiveExaminationsController(@Req() req: RequestWithUser) {
    try {
      return (await this.getActiveExaminations.execute(req.user.uuid)).map((e) =>
        this.examinationSerializer.serialize(e),
      );
    } catch (e) {
      if (e instanceof NotFoundException) throw e;
      else throw new BadRequestException(e.message);
    }
  }

  @Roles(Role.PRACTITIONER, Role.NURSE)
  @Get(':uuid')
  async getExaminationController(@Param() { uuid }: UuidDTO) {
    try {
      return this.examinationSerializer.serialize(await this.getExamination.execute(uuid));
    } catch (e) {
      if (e instanceof NotFoundException) throw e;
      else throw new BadRequestException(e.message);
    }
  }

  @Roles(Role.PRACTITIONER)
  @Post('conclude/:uuid')
  async concludeExaminationController(
    @Req() req: RequestWithUser,
    @Param() { uuid }: UuidDTO,
    @Body() concludeExaminationDto: ConcludeExaminationDTO,
  ) {
    try {
      await this.concludeExamination.execute(
        concludeExaminationDto.completionType,
        req.user.uuid,
        uuid,
        concludeExaminationDto,
      );
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }
}
