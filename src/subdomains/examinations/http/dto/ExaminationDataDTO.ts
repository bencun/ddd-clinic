import { Type } from 'class-transformer';
import { IsArray, IsString, MinLength, ValidateNested } from 'class-validator';
import { PrescriptionDTO } from './PrescriptionDTO';

export class ExaminationDataDTO {
  @IsString()
  @MinLength(1)
  anamnesis: string;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => PrescriptionDTO)
  prescriptions: PrescriptionDTO[];

  @IsArray()
  @IsString({ each: true })
  diagnoses: string[];

  @IsArray()
  @IsString({ each: true })
  chronicDiagnosesEstablished: string[];

  @IsArray()
  @IsString({ each: true })
  chronicDiagnosesRemoved: string[];

  @IsString()
  notes: string;
}
