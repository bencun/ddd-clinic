import { Type } from 'class-transformer';
import {
  IsIn,
  IsNumber,
  IsObject,
  IsString,
  Min,
  MinLength,
  ValidateNested,
} from 'class-validator';
import { TDoseUnit } from 'src/shared/valueObjects/MedicationSpecification';

class MedicationSpecDTO {
  @IsString()
  @IsIn(['mg', 'g', 'ml'])
  doseUnit: TDoseUnit;

  @IsNumber()
  @Min(0)
  doseAmount: number;
}

class MedicationDTO {
  @IsString()
  @MinLength(1)
  name: string;

  @IsObject()
  @ValidateNested()
  @Type(() => MedicationSpecDTO)
  spec: MedicationSpecDTO;
}

export class PrescriptionDTO {
  @IsString()
  notes: string;

  @IsObject()
  @ValidateNested()
  @Type(() => MedicationDTO)
  medication: MedicationDTO;
}
