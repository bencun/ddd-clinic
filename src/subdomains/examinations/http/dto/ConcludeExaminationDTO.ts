import { IsIn, IsString } from 'class-validator';
import { ExaminationDataDTO } from './ExaminationDataDTO';

export class ConcludeExaminationDTO extends ExaminationDataDTO {
  @IsString()
  @IsIn(['cancel', 'complete'])
  completionType: 'cancel' | 'complete';
}
