import { Examination as ExaminationDb } from '.prisma/client';
import { ClassProvider, Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/database/prisma.service';
import { EventsService } from 'src/events/events.service';
import { PersistenceType } from 'src/shared/types/PersistenceType';
import { Uuid } from 'src/shared/types/Uuid';
import { Diagnosis } from 'src/shared/valueObjects/Diagnosis';
import {
  PrescriptionsMapper,
  RawPrescriptionsArray,
} from 'src/shared/valueObjects/mappers/PrescriptionsMapper';
import { ExaminationStatus } from '../../constants/ExaminationStatus';
import { Examination } from '../../entities/Examination';
import { ExaminationData } from '../../valueObjects/ExaminationData';
import { IExaminationRepository } from './IExaminationRepository';

@Injectable()
class ExaminationRepository implements IExaminationRepository {
  constructor(
    private readonly prisma: PrismaService,
    private readonly eventsService: EventsService,
  ) {}

  async isValidVersion(entity: Examination): Promise<PersistenceType> {
    const existingExmWithUnique = await this.prisma.examination.findFirst({
      where: { appointmentUuid: entity.appointmentUuid, uuid: { not: entity.uuid } },
    });
    if (existingExmWithUnique) return PersistenceType.NOT_UNIQUE;
    const existingExm = await this.prisma.examination.findUnique({ where: { uuid: entity.uuid } });
    if (existingExm) {
      if (existingExm.version === entity.version) {
        return PersistenceType.VALID;
      } else {
        return PersistenceType.INVALID;
      }
    }
    return PersistenceType.NEW;
  }

  async saveExamination(examination: Examination): Promise<void> {
    const validity = await this.isValidVersion(examination);
    if (validity === PersistenceType.INVALID) {
      throw new Error('Examination version mismatch!');
    }
    if (validity === PersistenceType.NOT_UNIQUE) {
      throw new Error('Examination related to this appointment already exists!');
    }

    const examinationData = {
      anamnesis: examination?.ExaminationData?.props.anamnesis,
      notes: examination?.ExaminationData?.props.notes,
      prescriptions: examination?.ExaminationData?.props.prescriptions.map((p) =>
        PrescriptionsMapper.toPersistenceObject(p),
      ),
      diagnoses: examination?.ExaminationData?.props.diagnoses.map((d) => d.props.code),
      chronicDiagnosesEstablished:
        examination?.ExaminationData?.props.chronicDiagnosesEstablished.map((d) => d.props.code),
      chronicDiagnosesRemoved: examination?.ExaminationData?.props.chronicDiagnosesRemoved.map(
        (d) => d.props.code,
      ),
    };

    await this.prisma.examination.upsert({
      where: { uuid: examination.uuid },
      create: {
        uuid: examination.uuid,
        version: examination.version,
        appointmentUuid: examination.appointmentUuid,
        practitionerUuid: examination.practitionerUuid,
        patientUuid: examination.patientUuid,
        patientName: examination.patientName,
        status: examination.Status,
        knownChronicDiagnoses: examination.knownChronicDiagnoses.map((d) => d.props.code),
        startedAt: examination.startedAt,
        endedAt: examination?.endedAt,
        ...examinationData,
      },
      update: {
        uuid: examination.uuid,
        version: examination.version,
        appointmentUuid: examination.appointmentUuid,
        practitionerUuid: examination.practitionerUuid,
        patientUuid: examination.patientUuid,
        patientName: examination.patientName,
        status: examination.Status,
        knownChronicDiagnoses: examination.knownChronicDiagnoses.map((d) => d.props.code),
        startedAt: examination.startedAt,
        endedAt: examination?.endedAt,
        ...examinationData,
      },
    });

    this.eventsService.emitAll(examination.domainEvents);
  }

  async loadExaminationForAppointment(appointmentUuid: Uuid): Promise<Examination> {
    const examination = await this.prisma.examination.findUnique({
      where: { appointmentUuid },
      select: { uuid: true },
    });
    if (!examination) throw new NotFoundException();
    return await this.loadExamination(examination.uuid);
  }

  async loadExaminationsForPractitioner(practitionerUuid: Uuid): Promise<Examination[]> {
    const examinations = await this.prisma.examination.findMany({ where: { practitionerUuid } });
    return examinations.map((e) => this.mapDbToExamination(e));
  }

  private mapDbToExamination(eDb: ExaminationDb): Examination {
    return new Examination(
      eDb.appointmentUuid,
      eDb.practitionerUuid,
      eDb.patientUuid,
      eDb.patientName,
      eDb.knownChronicDiagnoses.map((d) => new Diagnosis(d)),
      eDb.startedAt,
      eDb.endedAt
        ? new ExaminationData(
            eDb.anamnesis,
            eDb.diagnoses.map((d) => new Diagnosis(d)),
            eDb.prescriptions
              ? (eDb.prescriptions as RawPrescriptionsArray).map((p) =>
                  PrescriptionsMapper.toValueObject(p),
                )
              : [],
            eDb.chronicDiagnosesEstablished.map((d) => new Diagnosis(d)),
            eDb.chronicDiagnosesRemoved.map((d) => new Diagnosis(d)),
          )
        : null,
      eDb.status as ExaminationStatus,
      eDb.endedAt,
      eDb.version,
      eDb.uuid,
    );
  }

  async loadExamination(uuid: string): Promise<Examination> {
    const eDb = await this.prisma.examination.findUnique({ where: { uuid } });
    if (!eDb) {
      throw new NotFoundException();
    }
    return this.mapDbToExamination(eDb);
  }
}

export const ExaminationRepositoryProvider: ClassProvider = {
  provide: IExaminationRepository,
  useClass: ExaminationRepository,
};
