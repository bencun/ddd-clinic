import { IRepository } from 'src/shared/ddd/IRepository';
import { PersistenceType } from 'src/shared/types/PersistenceType';
import { Uuid } from 'src/shared/types/Uuid';
import { Examination } from '../../entities/Examination';

export abstract class IExaminationRepository implements IRepository<Examination> {
  abstract isValidVersion(entity: Examination): Promise<PersistenceType>;
  abstract saveExamination(examination: Examination): Promise<void>;
  abstract loadExamination(uuid: Uuid): Promise<Examination>;
  abstract loadExaminationsForPractitioner(practitionerUuid: Uuid): Promise<Examination[]>;
  abstract loadExaminationForAppointment(appointmentUuid: Uuid): Promise<Examination>;
}
