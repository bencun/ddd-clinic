import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { DomainEvent } from 'src/shared/ddd/DomainEvent';
import { Examination } from '../entities/Examination';

export class ExaminationCanceledEvent extends DomainEvent {
  constructor(public readonly examination: Examination) {
    super(false);
  }

  public get token(): DomainEventToken {
    return DomainEventToken.EXAMINATION_CANCELED;
  }
}
