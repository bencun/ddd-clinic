import { Module } from '@nestjs/common';
import { ExaminationCompletedPatientHandler } from './handlers/ExaminationCompletedPatientHandler';
import { PatientsController } from './http/PatientsController';
import { PatientRepositoryProvider } from './persistence/PatientRepository/Provider';
import { HealthRecordSerializer } from './serializers/HealthRecordSerializer';
import { PatientSerializer } from './serializers/PatientSerializer';
import { AddHealthRecord } from './useCases/AddHealthRecord';
import { CreatePatient } from './useCases/CreatePatient';
import { GetAllPatients } from './useCases/GetAllPatients';
import { GetKnownChronicDiagnoses } from './useCases/GetKnownChronicDiagnoses';
import { GetPatient } from './useCases/GetPatient';
import { UpdatePatientProfile } from './useCases/UpdatePatientProfile';

@Module({
  controllers: [PatientsController],
  providers: [
    PatientRepositoryProvider,
    PatientSerializer,
    HealthRecordSerializer,
    CreatePatient,
    UpdatePatientProfile,
    GetPatient,
    GetAllPatients,
    GetKnownChronicDiagnoses,
    AddHealthRecord,
    ExaminationCompletedPatientHandler,
  ],
  exports: [GetKnownChronicDiagnoses],
})
export class PatientsModule {}
