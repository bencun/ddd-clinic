import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Address } from 'src/shared/valueObjects/Address';
import { Email } from 'src/shared/valueObjects/Email';
import { JMBGNumber } from 'src/shared/valueObjects/JMBGNumber';
import { LBONumber } from 'src/shared/valueObjects/LBONumber';
import { PhoneNumber } from 'src/shared/valueObjects/PhoneNumber';
import { CreatePatientDTO } from '../http/dto/CreatePatientDTO';
import { Patient } from '../entities/Patient';
import { IPatientRepository } from '../persistence/PatientRepository/IPatientRepository';
import { UuidDTO } from 'src/shared/dtos/UuidDTO';

@Injectable()
export class CreatePatient implements IUseCase<UuidDTO> {
  constructor(private readonly patientsRepo: IPatientRepository) {}

  async execute(patientDto: CreatePatientDTO): Promise<UuidDTO> {
    const { name, jmbg, lbo, email, phone, city, postalCode, street, number } = patientDto;
    const patient = new Patient(
      name,
      new JMBGNumber(jmbg),
      new LBONumber(lbo),
      new Email(email),
      new PhoneNumber(phone),
      new Address(city, postalCode, street, number),
      [],
      1,
    );
    await this.patientsRepo.savePatient(patient);
    return { uuid: patient.uuid };
  }
}
