import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Uuid } from 'src/shared/types/Uuid';
import { Diagnosis } from 'src/shared/valueObjects/Diagnosis';
import { IPatientRepository } from '../persistence/PatientRepository/IPatientRepository';
import { InferChronicDiagnoses } from '../utils/InferChronicDiagnoses';

@Injectable()
export class GetKnownChronicDiagnoses implements IUseCase<Diagnosis[]> {
  constructor(private readonly patientRepo: IPatientRepository) {}

  async execute(patientUuid: Uuid): Promise<Diagnosis[]> {
    const patient = await this.patientRepo.loadPatient(patientUuid);
    return InferChronicDiagnoses(patient.HealthRecords);
  }
}
