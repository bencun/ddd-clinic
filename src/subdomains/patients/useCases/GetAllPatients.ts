import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { AdvancedDataQueryDTO } from 'src/shared/dtos/AdvancedDataQueryDTO';
import { WrappedPaginationData } from 'src/shared/types/PaginationWrapper';
import { Patient } from '../entities/Patient';
import { IPatientRepository } from '../persistence/PatientRepository/IPatientRepository';

@Injectable()
export class GetAllPatients implements IUseCase<WrappedPaginationData<Patient>> {
  constructor(private readonly patientRepo: IPatientRepository) {}

  async execute(params: AdvancedDataQueryDTO): Promise<WrappedPaginationData<Patient>> {
    return await this.patientRepo.loadPatients(params);
  }
}
