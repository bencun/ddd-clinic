import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Uuid } from 'src/shared/types/Uuid';
import { Address } from 'src/shared/valueObjects/Address';
import { Email } from 'src/shared/valueObjects/Email';
import { JMBGNumber } from 'src/shared/valueObjects/JMBGNumber';
import { LBONumber } from 'src/shared/valueObjects/LBONumber';
import { PhoneNumber } from 'src/shared/valueObjects/PhoneNumber';
import { PatientProfileDTO } from '../http/dto/PatientProfileDTO';
import { IPatientRepository } from '../persistence/PatientRepository/IPatientRepository';

@Injectable()
export class UpdatePatientProfile implements IUseCase<void> {
  constructor(private readonly patientRepo: IPatientRepository) {}

  async execute(patientUuid: Uuid, profileDto: PatientProfileDTO): Promise<void> {
    const { name, jmbg, lbo, email, phone, city, street, postalCode, number } = profileDto;
    const patient = await this.patientRepo.loadPatient(patientUuid);
    patient.SetNewProfileInformation({
      name,
      jmbg: new JMBGNumber(jmbg),
      lbo: new LBONumber(lbo),
      email: new Email(email),
      phone: new PhoneNumber(phone),
      address: new Address(city, postalCode, street, number),
    });
    await this.patientRepo.savePatient(patient);
  }
}
