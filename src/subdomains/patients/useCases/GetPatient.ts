import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Uuid } from 'src/shared/types/Uuid';
import { Patient } from '../entities/Patient';
import { IPatientRepository } from '../persistence/PatientRepository/IPatientRepository';

@Injectable()
export class GetPatient implements IUseCase<Patient> {
  constructor(private readonly patientRepo: IPatientRepository) {}

  async execute(patientUuid: Uuid): Promise<Patient> {
    return await this.patientRepo.loadPatient(patientUuid);
  }
}
