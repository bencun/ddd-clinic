import { Injectable } from '@nestjs/common';
import { IUseCase } from 'src/shared/ddd/UseCase';
import { Uuid } from 'src/shared/types/Uuid';
import { HealthRecord } from '../entities/HealthRecord';
import { Patient } from '../entities/Patient';
import { IPatientRepository } from '../persistence/PatientRepository/IPatientRepository';

@Injectable()
export class AddHealthRecord implements IUseCase<Patient> {
  constructor(private readonly patientRepo: IPatientRepository) {}

  async execute(patientUuid: Uuid, hr: HealthRecord): Promise<Patient> {
    const patient = await this.patientRepo.loadPatient(patientUuid);
    patient.AddHealthRecord(hr);
    await this.patientRepo.savePatient(patient);
    return patient;
  }
}
