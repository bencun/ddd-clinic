import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { IEventHandler } from 'src/shared/ddd/IEventHandler';
import { ExaminationCompletedEvent } from 'src/subdomains/examinations/events/ExaminationCompletedEvent';
import { HealthRecord } from '../entities/HealthRecord';
import { AddHealthRecord } from '../useCases/AddHealthRecord';

@Injectable()
export class ExaminationCompletedPatientHandler
  implements IEventHandler<ExaminationCompletedEvent>
{
  constructor(private readonly addHealthRecord: AddHealthRecord) {}

  @OnEvent(DomainEventToken.EXAMINATION_COMPLETED)
  async handle({ examination }: ExaminationCompletedEvent): Promise<any> {
    await this.addHealthRecord.execute(
      examination.patientUuid,
      new HealthRecord(
        examination.uuid,
        examination.ExaminationData.props.diagnoses,
        examination.ExaminationData.props.chronicDiagnosesEstablished,
        examination.ExaminationData.props.chronicDiagnosesRemoved,
        examination.ExaminationData.props.prescriptions,
        1,
        new Date(),
      ),
    );
  }
}
