import { Diagnosis } from 'src/shared/valueObjects/Diagnosis';
import { HealthRecord } from '../entities/HealthRecord';

export function InferChronicDiagnoses(healthRecords: Readonly<HealthRecord[]>) {
  const diagnosesMap = new Map<string, Diagnosis>();
  // compute the latest state of chronic diagnoses since health records are sorted
  for (const hr of healthRecords) {
    // remove diagnoses
    for (const d of hr.chronicDiagnosesRemoved) {
      diagnosesMap.delete(d.props.code);
    }
    for (const d of hr.chronicDiagnosesEstablished) {
      diagnosesMap.set(d.props.code, d);
    }
  }
  // convert to array
  return [...diagnosesMap.entries()].map(([code]) => new Diagnosis(code));
}
