import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { DomainEvent } from 'src/shared/ddd/DomainEvent';
import { Uuid } from 'src/shared/types/Uuid';
import { ProfileInformation } from '../types/ProfileInformation';

export class PatientProfileUpdatedEvent extends DomainEvent {
  constructor(
    public readonly patientUuid: Uuid,
    public readonly profileInformation: Partial<ProfileInformation>,
  ) {
    super(true);
  }
  public get token(): DomainEventToken {
    return DomainEventToken.PATIENT_PROFILE_UPDATED;
  }
}
