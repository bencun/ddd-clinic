import { IsEmail, IsPhoneNumber, IsString, Length, MinLength } from 'class-validator';
import { AddressDTO } from './AddressDTO';

export class PatientProfileDTO extends AddressDTO {
  @IsString()
  @MinLength(1)
  name: string;

  @IsString()
  @Length(13, 13)
  jmbg: string;

  @IsString()
  @Length(11, 11)
  lbo: string;

  @IsString()
  @IsEmail()
  email: string;

  @IsString()
  @IsPhoneNumber()
  phone: string;
}
