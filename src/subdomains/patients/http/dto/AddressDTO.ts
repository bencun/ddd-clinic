import { IsString, MinLength } from 'class-validator';

export class AddressDTO {
  @IsString()
  @MinLength(2)
  city: string;

  @IsString()
  @MinLength(3)
  postalCode: string;

  @IsString()
  @MinLength(1)
  street: string;

  @IsString()
  @MinLength(1)
  number: string;
}
