import { Role } from '.prisma/client';
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { UuidDTO } from 'src/shared/dtos/UuidDTO';
import { CreatePatientDTO } from './dto/CreatePatientDTO';
import { PatientProfileDTO } from './dto/PatientProfileDTO';
import { PatientSerializer } from '../serializers/PatientSerializer';
import { CreatePatient } from '../useCases/CreatePatient';
import { GetPatient } from '../useCases/GetPatient';
import { UpdatePatientProfile } from '../useCases/UpdatePatientProfile';
import { AdvancedDataQueryDTO } from 'src/shared/dtos/AdvancedDataQueryDTO';
import { GetAllPatients } from '../useCases/GetAllPatients';
import { WrappedPaginationData } from 'src/shared/types/PaginationWrapper';
import { PatientResponseDTO } from '../serializers/types/PatientResponseDTO';

@Controller('patients')
export class PatientsController {
  constructor(
    private readonly createPatient: CreatePatient,
    private readonly updatePatientProfile: UpdatePatientProfile,
    private readonly getPatient: GetPatient,
    private readonly getAllPatients: GetAllPatients,
    private readonly patientSerializer: PatientSerializer,
  ) {}

  @Roles(Role.NURSE, Role.PRACTITIONER)
  @Get('')
  async getAllPatientsController(
    @Query() params: AdvancedDataQueryDTO,
  ): Promise<WrappedPaginationData<PatientResponseDTO>> {
    const wrapper = await this.getAllPatients.execute(params);
    return {
      ...wrapper,
      data: wrapper.data.map((p) => this.patientSerializer.serialize(p)),
    };
  }

  @Roles(Role.NURSE, Role.PRACTITIONER)
  @Get(':uuid')
  async getPatientController(@Param() { uuid }: UuidDTO) {
    return this.patientSerializer.serialize(await this.getPatient.execute(uuid));
  }

  @Roles(Role.NURSE)
  @Post()
  async createPatientController(@Body() createPatientDto: CreatePatientDTO) {
    try {
      return await this.createPatient.execute(createPatientDto);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }

  @Roles(Role.NURSE)
  @Put(':uuid')
  async updatePatientProfileController(
    @Param() { uuid }: UuidDTO,
    @Body() updateProfileDto: PatientProfileDTO,
  ) {
    try {
      return await this.updatePatientProfile.execute(uuid, updateProfileDto);
    } catch (e) {
      throw new BadRequestException(e.message);
    }
  }
}
