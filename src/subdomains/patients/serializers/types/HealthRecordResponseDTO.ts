import { RawPrescriptionsArray } from 'src/shared/valueObjects/mappers/PrescriptionsMapper';

export type HealthRecordResponseDTO = {
  uuid: string;
  date: string;
  examinationUuid: string;
  diagnosesEstablished: string[];
  chronicDiagnosesEstablished: string[];
  chronicDiagnosesRemoved: string[];
  prescriptions: RawPrescriptionsArray;
};
