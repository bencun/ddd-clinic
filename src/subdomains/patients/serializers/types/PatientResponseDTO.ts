import { HealthRecordResponseDTO } from './HealthRecordResponseDTO';

export type PatientResponseDTO = {
  uuid: string;
  name: string;
  jmbg: string;
  lbo: string;
  email: string;
  phone: string;
  city: string;
  postalCode: string;
  street: string;
  number: string;
  healthRecords: HealthRecordResponseDTO[];
};
