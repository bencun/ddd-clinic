import { Injectable } from '@nestjs/common';
import { IEntitySerializer } from 'src/shared/ddd/IEntitySerializer';
import { Patient } from '../entities/Patient';
import { HealthRecordSerializer } from './HealthRecordSerializer';
import { PatientResponseDTO } from './types/PatientResponseDTO';

@Injectable()
export class PatientSerializer implements IEntitySerializer<Patient, PatientResponseDTO> {
  constructor(private readonly healthRecordSerializer: HealthRecordSerializer) {}

  serialize(patient: Patient): PatientResponseDTO {
    return {
      uuid: patient.uuid,
      name: patient.Profile.name,
      jmbg: patient.Profile.jmbg.props.value,
      lbo: patient.Profile.lbo.props.value,
      email: patient.Profile.email.props.value,
      phone: patient.Profile.phone.props.value,
      city: patient.Profile.address.props.city,
      postalCode: patient.Profile.address.props.postalCode,
      street: patient.Profile.address.props.street,
      number: patient.Profile.address.props.number,
      healthRecords: patient.HealthRecords.map((hr) => this.healthRecordSerializer.serialize(hr)),
    };
  }
}
