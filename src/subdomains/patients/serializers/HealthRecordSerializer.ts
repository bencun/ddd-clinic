import { Injectable } from '@nestjs/common';
import { formatISO } from 'date-fns';
import { IEntitySerializer } from 'src/shared/ddd/IEntitySerializer';
import { PrescriptionsMapper } from 'src/shared/valueObjects/mappers/PrescriptionsMapper';
import { HealthRecord } from '../entities/HealthRecord';
import { HealthRecordResponseDTO } from './types/HealthRecordResponseDTO';

@Injectable()
export class HealthRecordSerializer
  implements IEntitySerializer<HealthRecord, HealthRecordResponseDTO>
{
  serialize(hr: HealthRecord): HealthRecordResponseDTO {
    return {
      uuid: hr.uuid,
      date: formatISO(hr.date),
      examinationUuid: hr.examinationUuid,
      diagnosesEstablished: hr.diagnosesEstablished.map((d) => d.props.code),
      chronicDiagnosesRemoved: hr.chronicDiagnosesRemoved.map((d) => d.props.code),
      chronicDiagnosesEstablished: hr.chronicDiagnosesEstablished.map((d) => d.props.code),
      prescriptions: hr.prescriptions.map((p) => PrescriptionsMapper.toPersistenceObject(p)),
    };
  }
}
