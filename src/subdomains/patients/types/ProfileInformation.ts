import { Address } from 'src/shared/valueObjects/Address';
import { Email } from 'src/shared/valueObjects/Email';
import { JMBGNumber } from 'src/shared/valueObjects/JMBGNumber';
import { LBONumber } from 'src/shared/valueObjects/LBONumber';
import { PhoneNumber } from 'src/shared/valueObjects/PhoneNumber';

export type ProfileInformation = {
  name: string;
  jmbg: JMBGNumber;
  lbo: LBONumber;
  email: Email;
  phone: PhoneNumber;
  address: Address;
};
