import { IRepository } from 'src/shared/ddd/IRepository';
import { PersistenceType } from 'src/shared/types/PersistenceType';
import { Uuid } from 'src/shared/types/Uuid';
import { Patient } from '../../entities/Patient';
import { AdvancedDataQueryDTO } from 'src/shared/dtos/AdvancedDataQueryDTO';
import { WrappedPaginationData } from 'src/shared/types/PaginationWrapper';

export abstract class IPatientRepository implements IRepository<Patient> {
  abstract isValidVersion(entity: Patient): Promise<PersistenceType>;
  abstract loadPatient(patientUuid: Uuid): Promise<Patient>;
  abstract loadPatients(params: AdvancedDataQueryDTO): Promise<WrappedPaginationData<Patient>>;
  abstract savePatient(patient: Patient): Promise<void>;
}
