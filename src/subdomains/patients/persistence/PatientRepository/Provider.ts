import {
  Prisma,
  Patient as PrismaPatientModel,
  HealthRecord as PrismaHealthRecordModel,
} from '.prisma/client';
import { BadRequestException, ClassProvider, Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/database/prisma.service';
import { EventsService } from 'src/events/events.service';
import { AdvancedDataQueryDTO } from 'src/shared/dtos/AdvancedDataQueryDTO';
import { contains } from 'src/shared/helpers/ContainsQueryHelper';
import { WrappedPaginationData } from 'src/shared/types/PaginationWrapper';
import { PersistenceType } from 'src/shared/types/PersistenceType';
import { Address } from 'src/shared/valueObjects/Address';
import { Email } from 'src/shared/valueObjects/Email';
import { JMBGNumber } from 'src/shared/valueObjects/JMBGNumber';
import { LBONumber } from 'src/shared/valueObjects/LBONumber';
import { HealthRecordMapper } from 'src/shared/valueObjects/mappers/HealthRecordMapper';
import { PrescriptionsMapper } from 'src/shared/valueObjects/mappers/PrescriptionsMapper';
import { PhoneNumber } from 'src/shared/valueObjects/PhoneNumber';
import { Patient } from '../../entities/Patient';
import { IPatientRepository } from './IPatientRepository';

@Injectable()
class PatientRepository implements IPatientRepository {
  constructor(
    private readonly prisma: PrismaService,
    private readonly eventsService: EventsService,
  ) {}

  async isValidVersion(entity: Patient): Promise<PersistenceType> {
    const existingPatient = await this.prisma.patient.findUnique({
      where: { uuid: entity.uuid },
    });
    const uniquePatientMatches = await this.prisma.patient.findMany({
      where: {
        OR: [{ jmbg: entity.Profile.jmbg.props.value }, { lbo: entity.Profile.lbo.props.value }],
      },
    });
    if (existingPatient) {
      return existingPatient.version === entity.version
        ? PersistenceType.VALID
        : PersistenceType.INVALID;
    } else if (uniquePatientMatches.length > 0) {
      return PersistenceType.NOT_UNIQUE;
    }
    return PersistenceType.NEW;
  }

  private patientPersistenceToPatient(
    model: PrismaPatientModel & {
      healthRecords: PrismaHealthRecordModel[];
    },
  ): Patient {
    return new Patient(
      model.name,
      new JMBGNumber(model.jmbg),
      new LBONumber(model.lbo),
      new Email(model.email),
      new PhoneNumber(model.phone),
      new Address(model.city, model.postalCode, model.street, model.number),
      model.healthRecords.map((hr) => HealthRecordMapper.toValueObject(hr)),
      model.version,
      model.uuid,
    );
  }

  async loadPatients(queryParams: AdvancedDataQueryDTO): Promise<WrappedPaginationData<Patient>> {
    const where: Prisma.PatientWhereInput = {
      AND: {
        OR: [
          { email: contains(queryParams.text) },
          { name: contains(queryParams.text) },
          { jmbg: contains(queryParams.text) },
          { lbo: contains(queryParams.text) },
          { phone: contains(queryParams.text) },
          { city: contains(queryParams.text) },
          { street: contains(queryParams.text) },
        ],
      },
    };

    const count = await this.prisma.patient.count({ where });
    const patients = (
      await this.prisma.patient.findMany({
        where,
        ...queryParams.BuildPrismaPaginationParams(),
        ...queryParams.BuildPrismaOrderingParams(),
        include: {
          healthRecords: {
            orderBy: {
              date: Prisma.SortOrder.asc,
            },
          },
        },
      })
    ).map((p) => this.patientPersistenceToPatient(p));
    return {
      data: patients,
      count,
      skip: queryParams.skip,
      take: queryParams.take,
    };
  }

  async loadPatient(patientUuid: string): Promise<Patient> {
    const patient = await this.prisma.patient.findUnique({
      where: { uuid: patientUuid },
      include: {
        healthRecords: true,
      },
    });

    if (!patient) {
      throw new NotFoundException();
    }

    return this.patientPersistenceToPatient(patient);
  }

  async savePatient(patient: Patient): Promise<void> {
    const validity = await this.isValidVersion(patient);
    switch (validity) {
      case PersistenceType.INVALID:
        throw new BadRequestException('Patient version mismatch.');
      case PersistenceType.NOT_UNIQUE:
        throw new BadRequestException('Patient with this JMBG or LBO already exists.');
    }
    // transactional update
    const patientQueryParams: Prisma.PatientCreateInput = {
      name: patient.Profile.name,
      phone: patient.Profile.phone.props.value,
      email: patient.Profile.email.props.value,
      jmbg: patient.Profile.jmbg.props.value,
      lbo: patient.Profile.lbo.props.value,
      city: patient.Profile.address.props.city,
      postalCode: patient.Profile.address.props.postalCode,
      street: patient.Profile.address.props.street,
      number: patient.Profile.address.props.number,
      version: patient.version,
    };
    const patientUpdatePromise = this.prisma.patient.upsert({
      where: { uuid: patient.uuid },
      create: {
        ...patientQueryParams,
        uuid: patient.uuid,
      },
      update: {
        ...patientQueryParams,
        version: { increment: 1 },
      },
    });
    const healthRecordUpdatePromises = patient.HealthRecords.map((hr) =>
      this.prisma.healthRecord.upsert({
        where: { uuid: hr.uuid },
        create: {
          uuid: hr.uuid,
          date: hr.date,
          examinationUuid: hr.examinationUuid,
          prescriptions: hr.prescriptions.map((p) => PrescriptionsMapper.toPersistenceObject(p)),
          diagnosesEstablished: hr.diagnosesEstablished.map((d) => d.props.code),
          chronicDiagnosesEstablished: hr.chronicDiagnosesEstablished.map((d) => d.props.code),
          chronicDiagnosesRemoved: hr.chronicDiagnosesRemoved.map((d) => d.props.code),
          patientUuid: patient.uuid,
        },
        update: {
          examinationUuid: hr.examinationUuid,
          prescriptions: hr.prescriptions.map((p) => PrescriptionsMapper.toPersistenceObject(p)),
          diagnosesEstablished: hr.diagnosesEstablished.map((d) => d.props.code),
          chronicDiagnosesEstablished: hr.chronicDiagnosesEstablished.map((d) => d.props.code),
          chronicDiagnosesRemoved: hr.chronicDiagnosesRemoved.map((d) => d.props.code),
          version: { increment: 1 },
        },
      }),
    );

    await this.prisma.$transaction([patientUpdatePromise, ...healthRecordUpdatePromises]);
    this.eventsService.emitAll(patient.domainEvents);
  }
}

export const PatientRepositoryProvider: ClassProvider = {
  provide: IPatientRepository,
  useClass: PatientRepository,
};
