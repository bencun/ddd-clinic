import { BaseEntity } from 'src/shared/ddd/BaseEntity';
import { Uuid } from 'src/shared/types/Uuid';
import { Diagnosis } from 'src/shared/valueObjects/Diagnosis';
import { Prescription } from 'src/shared/valueObjects/Prescription';

export class HealthRecord extends BaseEntity {
  public readonly date: Date;
  public readonly examinationUuid: Uuid;
  public readonly diagnosesEstablished: Diagnosis[];
  public readonly chronicDiagnosesEstablished: Diagnosis[];
  public readonly chronicDiagnosesRemoved: Diagnosis[];
  public readonly prescriptions: Prescription[];

  constructor(
    examinationUuid: Uuid,
    diagnosesEstablished: Diagnosis[],
    chronicDiagnosesEstablished: Diagnosis[],
    chronicDiagnosesRemoved: Diagnosis[],
    prescriptions: Prescription[],
    version: number,
    date: Date,
    uuid?: Uuid,
  ) {
    super(version, uuid);
    this.date = date;
    this.examinationUuid = examinationUuid;
    this.diagnosesEstablished = diagnosesEstablished;
    this.chronicDiagnosesEstablished = chronicDiagnosesEstablished;
    this.chronicDiagnosesRemoved = chronicDiagnosesRemoved;
    this.prescriptions = prescriptions;
  }
}
