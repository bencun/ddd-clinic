import { isBefore } from 'date-fns';
import { AggregateRoot } from 'src/shared/ddd/AggregateRoot';
import { Uuid } from 'src/shared/types/Uuid';
import { Address } from 'src/shared/valueObjects/Address';
import { Email } from 'src/shared/valueObjects/Email';
import { JMBGNumber } from 'src/shared/valueObjects/JMBGNumber';
import { LBONumber } from 'src/shared/valueObjects/LBONumber';
import { PhoneNumber } from 'src/shared/valueObjects/PhoneNumber';
import { PatientProfileUpdatedEvent } from '../events/PatientProfileUpdatedEvent';
import { ProfileInformation } from '../types/ProfileInformation';
import { HealthRecord } from './HealthRecord';

export class Patient extends AggregateRoot {
  constructor(
    private _name: string,
    private _jmbg: JMBGNumber,
    private _lbo: LBONumber,
    private _email: Email,
    private _phone: PhoneNumber,
    private _address: Address,
    private healthRecords: HealthRecord[],
    version: number,
    uuid?: Uuid,
  ) {
    super(version, uuid);
  }

  public SetNewProfileInformation(profile: Partial<ProfileInformation>) {
    if (profile.name) {
      this._name = profile.name;
    }
    if (profile.jmbg) {
      this._jmbg = profile.jmbg;
    }
    if (profile.lbo) {
      this._lbo = profile.lbo;
    }
    if (profile.email) {
      this._email = profile.email;
    }
    if (profile.phone) {
      this._phone = profile.phone;
    }
    if (profile.address) {
      this._address = profile.address;
    }
    this.addEvent(new PatientProfileUpdatedEvent(this.uuid, profile));
  }

  get Profile(): Readonly<ProfileInformation> {
    return {
      name: this._name,
      jmbg: this._jmbg,
      lbo: this._lbo,
      email: this._email,
      phone: this._phone,
      address: this._address,
    } as const;
  }

  public AddHealthRecord(hr: HealthRecord) {
    if (!this.healthRecords.find((h) => h.uuid === hr.uuid)) {
      this.healthRecords.push(hr);
    } else {
      throw new Error('Health record already present.');
    }
  }

  get HealthRecords() {
    return [...this.healthRecords].sort((a1, a2) =>
      isBefore(a1.date, a2.date) ? -1 : 1,
    ) as ReadonlyArray<HealthRecord>;
  }
}
