import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Prisma, User } from '@prisma/client';
import { PrismaService } from 'src/database/prisma.service';
import { ActivateUserDTO } from '../dtos/ActivateUserDTO';
import bcrypt from 'bcrypt';
import { EmailChangedNotificationsService } from 'src/email/email-changed-notifications.service';
import { UsersService } from '../users.service';
import { PrepareEmailChangedNotificationData } from './helpers/PrepareEmailChangedNotificationData';
import { ForgotPasswordNotificationsService } from 'src/email/forgot-password-notifications.service';
import { ResetPasswordDTO } from './dtos/ResetPasswordDTO';
import { AuthService } from 'src/auth/auth.service';
@Injectable()
export class ActivationService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly jwtService: JwtService,
    private readonly authService: AuthService,
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly emailChangedNotificationService: EmailChangedNotificationsService,
    private readonly forgotPasswordNotificationService: ForgotPasswordNotificationsService,
  ) {}

  async generateActivationToken(uuid) {
    return await this.jwtService.signAsync({ sub: uuid });
  }

  async getUserFromPasswordResetToken(token: string, activated?: boolean) {
    try {
      const validation = await this.jwtService.verifyAsync(token);
      if (validation && validation.sub) {
        const user = await this.prisma.user.findFirst({
          where: { uuid: validation.sub, activated, resetToken: token },
        });
        if (user) {
          return user;
        }
      }
    } catch (e) {}
    // throw an exception if anything goes wrong
    throw new UnauthorizedException();
  }

  async activateNewUser(token: string, activateUserDto: ActivateUserDTO) {
    // try grabbing the user
    const user = await this.getUserFromPasswordResetToken(token, false);
    // check password fields since doing this in the DTO is complex
    if (activateUserDto.password !== activateUserDto.passwordConfirm) {
      throw new BadRequestException('Passwords do not match');
    }
    // prepare the data for Prisma
    const data: Prisma.UserUpdateInput = {
      activated: true,
      resetToken: null,
      password: bcrypt.hashSync(activateUserDto.password, 10),
    };
    // go
    const updatedUser = await this.prisma.user.update({
      data,
      where: {
        uuid: user.uuid,
      },
    });
    // middleware doesn't delete the password for User updates
    delete updatedUser.password;
    return updatedUser;
  }

  async changeEmail(uuid: User['uuid'], email: string) {
    const updatedUser = await this.prisma.user.update({
      where: { uuid },
      data: {
        email,
      },
    });
    // fire an email change notification asynchronously
    this.emailChangedNotificationService.sendNotification(
      PrepareEmailChangedNotificationData(updatedUser),
    );
    return updatedUser;
  }

  async resetOrForgotPasswordStart(email: string, resetPassword: boolean) {
    // find the user
    const user = await this.prisma.user.findUnique({ where: { email } });
    if (!user) {
      throw new NotFoundException();
    }
    // generate the reset token
    const resetToken = await this.generateActivationToken(user.uuid);
    // insert in the db
    await this.prisma.user.update({
      where: { email },
      data: { resetToken, password: resetPassword ? '' : undefined },
    });
    // send an email with a new token asynchronously
    this.forgotPasswordNotificationService.sendNotification({
      resetToken,
      to: email,
      resetPassword,
    });
  }

  async resetOrForgotPasswordVerify(resetPasswordDto: ResetPasswordDTO) {
    const user = await this.getUserFromPasswordResetToken(resetPasswordDto.token, true);
    return await this.authService.changeUserPassword(user.uuid, resetPasswordDto);
  }
}
