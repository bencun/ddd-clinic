import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { Role } from '@prisma/client';
import { PublicRoute } from 'src/auth/decorators/public-route.decorator';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { UuidDTO } from 'src/shared/dtos/UuidDTO';
import { ActivateUserDTO } from '../dtos/ActivateUserDTO';
import { ActivationTokenParamsDTO } from '../dtos/ActivationTokenParamsDTO';
import { ChangeEmailDTO } from '../dtos/ChangeEmailDTO';
import { ActivationService } from './activation.service';
import { ForgotPasswordDTO } from './dtos/ForgotPasswordDTO';
import { ResetPasswordDTO } from './dtos/ResetPasswordDTO';

@Controller('activation')
export class ActivationController {
  constructor(private readonly activationService: ActivationService) {}
  /** Get the user profile activation data */
  @PublicRoute()
  @Get('token/:token')
  async getUserForActivation(@Param() { token }: ActivationTokenParamsDTO) {
    return await this.activationService.getUserFromPasswordResetToken(token);
  }

  /** Activate the user using the profile data and the token */
  @PublicRoute()
  @Post('activate/:token')
  async activateUser(
    @Param() { token }: ActivationTokenParamsDTO,
    @Body() activateUserDto: ActivateUserDTO,
  ) {
    return await this.activationService.activateNewUser(token, activateUserDto);
  }

  @Roles(Role.ADMIN)
  @Post(':uuid/changeEmail')
  async changeEmail(@Body() { email }: ChangeEmailDTO, @Param() { uuid }: UuidDTO) {
    return this.activationService.changeEmail(uuid, email);
  }

  @Roles(Role.ADMIN)
  @Post('resetPasswordStart')
  async resetPasswordStart(@Body() { email }: ForgotPasswordDTO) {
    return await this.activationService.resetOrForgotPasswordStart(email, true);
  }

  @PublicRoute()
  @Post('forgotPasswordStart')
  async forgotPasswordStart(@Body() { email }: ForgotPasswordDTO) {
    return await this.activationService.resetOrForgotPasswordStart(email, false);
  }

  @PublicRoute()
  @Post('resetOrForgotPasswordVerify')
  async resetOrForgotPasswordVerify(@Body() resetPasswordDto: ResetPasswordDTO) {
    return await this.activationService.resetOrForgotPasswordVerify(resetPasswordDto);
  }
}
