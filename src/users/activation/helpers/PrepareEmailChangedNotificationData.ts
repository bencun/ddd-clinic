import { EmailChangedNotificationData } from 'src/email/types/EmailChangedNotificationData';
import { UserSanitized } from 'src/shared/types/UserSanitized';

export function PrepareEmailChangedNotificationData(
  user: UserSanitized,
): EmailChangedNotificationData {
  return {
    to: {
      email: user.email,
      name: `${user.name}`,
    },
  };
}
