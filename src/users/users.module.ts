import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { EmailModule } from 'src/email/email.module';
import { ActivationController } from './activation/activation.controller';
import { ActivationService } from './activation/activation.service';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  controllers: [UsersController, ActivationController],
  providers: [UsersService, ActivationService],
  imports: [
    // register another JWT module to use exclusively to generate password reset tokens
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_RESET_TOKEN_SECRET'),
        signOptions: {
          expiresIn: `${configService.get<number>('JWT_RESET_TOKEN_MAX_AGE')}s`,
        },
      }),
    }),

    EmailModule,
  ],
  exports: [UsersService],
})
export class UsersModule {}
