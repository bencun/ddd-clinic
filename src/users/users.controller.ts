import { Role } from '.prisma/client';
import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Query } from '@nestjs/common';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { UuidDTO } from 'src/shared/dtos/UuidDTO';
import { CreateUserDTO } from './dtos/CreateUserDTO';
import { UpdateProfileDTO } from './dtos/UpdateProfileDTO';
import { UserQueryParamsDTO } from './dtos/UserQueryParamsDTO';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Roles(Role.ADMIN)
  @Get(':uuid')
  async getOne(@Param() { uuid }: UuidDTO) {
    return await this.usersService.getOne(uuid);
  }

  @Roles(Role.ADMIN)
  @Get()
  async getAll(@Query() searchParams: UserQueryParamsDTO) {
    const paginatedUsers = await this.usersService.getManyQueried(
      searchParams.BuildPrismaPaginationParams(),
      searchParams.BuildPrismaOrderingParams(),
      searchParams.role,
      searchParams.text,
      searchParams.activated,
      searchParams.deleted,
    );
    return paginatedUsers;
  }

  @Roles(Role.ADMIN)
  @Post()
  async create(@Body() createUserDto: CreateUserDTO) {
    return await this.usersService.createUser(createUserDto);
  }

  @Roles(Role.ADMIN)
  @Put(':uuid')
  async updateProfile(@Body() updateProfileDTO: UpdateProfileDTO, @Param() { uuid }: UuidDTO) {
    return await this.usersService.updateProfile(uuid, updateProfileDTO);
  }

  @Roles(Role.ADMIN)
  @HttpCode(204)
  @Delete(':uuid')
  async deleteOne(@Param() { uuid }: UuidDTO) {
    return await this.usersService.deleteUser(uuid);
  }
}
