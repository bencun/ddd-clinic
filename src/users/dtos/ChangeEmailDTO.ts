import { IsEmail, IsNotEmpty, MaxLength } from 'class-validator';

export class ChangeEmailDTO {
  @IsNotEmpty()
  @MaxLength(100)
  @IsEmail()
  email: string;
}
