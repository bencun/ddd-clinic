import { IsNotEmpty, IsString } from 'class-validator';

export class ActivationTokenParamsDTO {
  @IsNotEmpty()
  @IsString()
  token: string;
}
