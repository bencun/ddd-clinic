import { Role } from '.prisma/client';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEnum, IsOptional } from 'class-validator';
import { AdvancedDataQueryDTO } from 'src/shared/dtos/AdvancedDataQueryDTO';

export class UserQueryParamsDTO extends AdvancedDataQueryDTO {
  @IsOptional()
  @IsEnum(Role)
  role?: Role;

  @IsOptional()
  @Transform(({ value }) => (value === 'true' ? true : false))
  @IsBoolean()
  activated?: boolean;

  @IsOptional()
  @Transform(({ value }) => (value === 'true' ? true : false))
  @IsBoolean()
  deleted?: boolean;
}
