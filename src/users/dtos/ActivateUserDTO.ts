import { IsNotEmpty, IsString, MinLength } from 'class-validator';

export class ActivateUserDTO {
  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  password: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  passwordConfirm: string;
}
