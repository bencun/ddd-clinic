import { Prisma, Role, User } from '.prisma/client';
import {
  ConflictException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from 'src/database/prisma.service';
import { WrappedPaginationData } from 'src/shared/types/PaginationWrapper';
import { PrismaOrderingParams, PrismaPaginationParams } from 'src/shared/types/DataQuerySubtypes';
import { CreateUserDTO } from './dtos/CreateUserDTO';
import { v4 as uuid } from 'uuid';
import { UpdateProfileDTO } from './dtos/UpdateProfileDTO';
import { ActivationNotificationsService } from 'src/email/activation-notifications.service';
import { ActivationService } from './activation/activation.service';
import { Uuid } from 'src/shared/types/Uuid';

@Injectable()
export class UsersService {
  constructor(
    private readonly prisma: PrismaService,
    @Inject(forwardRef(() => ActivationService))
    private readonly activationService: ActivationService,
    private readonly activationNotificationsService: ActivationNotificationsService,
  ) {}

  /** Gets a single user based on filtering params provided */
  async getOne(uuid: User['uuid']) {
    const user = await this.prisma.user.findUnique({ where: { uuid } });
    if (user) return user;
    else throw new NotFoundException();
  }

  /** Gets many users based on filtering params provided
   * @todo Add a flag to include/exclude actual profile data parametrically.
   */
  async getManyQueried(
    paginationParams: PrismaPaginationParams,
    orderingParams: PrismaOrderingParams,
    role?: Role,
    searchText?: string,
    activated?: boolean,
    deleted?: boolean,
  ): Promise<WrappedPaginationData<User>> {
    const where: Prisma.UserWhereInput = {
      AND: {
        OR: [
          { role },
          { deleted },
          { activated },
          {
            OR: [
              { email: { contains: searchText, mode: 'insensitive' } },
              { name: { contains: searchText, mode: 'insensitive' } },
            ],
          },
        ],
      },
    };

    const count = await this.prisma.user.count({
      where,
    });
    const users = await this.prisma.user.findMany({
      ...paginationParams,
      ...orderingParams,
      where,
    });

    return {
      data: users,
      count,
      skip: paginationParams.skip,
      take: paginationParams.take,
    };
  }

  /** Validates and creates a new user
   * @todo Move the token creation into a separate function and send an email!
   */
  async createUser(user: CreateUserDTO): Promise<User> {
    try {
      const userUuid = uuid();
      const createdUser = await this.prisma.user.create({
        data: {
          email: user.email,
          name: user.name,
          role: user.role,
          phone: user.phone,
          uuid: userUuid,
          password: '',
          activated: false,
          resetToken: await this.activationService.generateActivationToken(userUuid),
        },
      });
      // send an email asynchronously
      this.sendActivationEmail(createdUser);
      // return the created user
      return createdUser;
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        if (e.code === 'P2002') {
          throw new ConflictException('A user with this email already exists.');
        }
      }
      // rethrow if no condition is met
      throw e;
    }
  }

  private async sendActivationEmail(user: User) {
    this.activationNotificationsService.sendNotification({
      to: user.email,
      token: user.resetToken,
    });
  }

  /** Updates the user profile data */
  async updateProfile(userUuid: Uuid, updateProfileDto: UpdateProfileDTO): Promise<User> {
    const { email, name, phone } = updateProfileDto;
    await this.prisma.user.update({
      where: { uuid: userUuid },
      data: { email, name, phone },
    });
    return this.prisma.user.findUnique({ where: { uuid: userUuid } });
  }

  async deleteUser(userUuid: User['uuid']) {
    try {
      return await this.prisma.user.delete({ where: { uuid: userUuid } });
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
