export abstract class ValueObject<T extends Record<string, any>> {
  constructor(public readonly props: T) {}

  equals(otherObject: ValueObject<T>): boolean {
    // reference equality
    if (this === otherObject) return true;
    // null/undefined safety
    if (typeof otherObject === 'undefined' || otherObject === null) return false;
    // true structural equality
    for (const key of Object.keys(this.props)) {
      const currentValue = this.props[key];
      const otherValue = otherObject.props[key];
      // check if it's a value object
      if (currentValue instanceof ValueObject) {
        if (otherValue instanceof ValueObject) {
          if (!currentValue.equals(otherValue)) return false;
        } else return false;
        // otherwise check for primitive values' equality
      } else if (this.props[key] !== otherObject.props[key]) return false;
    }
    return true;
  }
}
