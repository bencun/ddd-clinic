import { BaseEntity } from './BaseEntity';
import { DomainEvent } from './DomainEvent';
import { Uuid } from '../types/Uuid';

export abstract class AggregateRoot extends BaseEntity {
  private events: DomainEvent[] = [];

  constructor(version: number, uuid?: Uuid) {
    super(version, uuid);
  }

  protected addEvent(event: DomainEvent) {
    this.events.push(event);
  }

  get domainEvents() {
    return [...this.events] as ReadonlyArray<DomainEvent>;
  }
}
