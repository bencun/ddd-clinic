export abstract class IUseCase<T> {
  abstract execute(...args: any[]): Promise<T>;
}
