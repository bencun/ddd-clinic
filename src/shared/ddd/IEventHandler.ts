import { DomainEvent } from './DomainEvent';

export interface IEventHandler<T extends DomainEvent> {
  handle(event: T): Promise<any>;
}
