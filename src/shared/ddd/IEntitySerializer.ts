import { BaseEntity } from './BaseEntity';

export interface IEntitySerializer<E extends BaseEntity, S> {
  serialize(entity: E): S;
}
