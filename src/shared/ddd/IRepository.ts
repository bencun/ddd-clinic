import { PersistenceType } from '../types/PersistenceType';
import { AggregateRoot } from './AggregateRoot';

/**
 * Marker interface for repositories
 */
export abstract class IRepository<T extends AggregateRoot> {
  public abstract isValidVersion(entity: T): Promise<PersistenceType>;
}
