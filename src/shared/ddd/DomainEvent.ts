import { DomainEventToken } from '../constants/DomainEventToken';

export abstract class DomainEvent {
  public abstract get token(): DomainEventToken;
  public readonly timestamp: Date = new Date();

  constructor(public readonly transactional = false) {}
}
