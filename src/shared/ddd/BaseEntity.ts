import { v4 as newUuid } from 'uuid';
import { Uuid } from '../types/Uuid';

export abstract class BaseEntity {
  constructor(public readonly version: number, public readonly uuid: Uuid = newUuid()) {}
}
