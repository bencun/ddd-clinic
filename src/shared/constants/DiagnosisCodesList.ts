import { DiagnosisCodesMap } from './DiagnosisCodesMap';

export const DiagnosisCodesList = [...DiagnosisCodesMap.entries()].map(([code, name]) => ({
  code,
  name,
}));
