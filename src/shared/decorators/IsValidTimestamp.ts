import { IsNotEmpty, IsDate } from 'class-validator';
import { Transform } from 'class-transformer';
import { ValidatorComposerFactory } from './ValidatorComposerFactory';
import { parseISO } from 'date-fns';

export function IsValidTimestamp() {
  return ValidatorComposerFactory([
    IsNotEmpty(),
    Transform(({ value }) => {
      const date = parseISO(value);
      if (!Number.isNaN(date.getTime())) return date;
    }),
    IsDate(),
  ]);
}
