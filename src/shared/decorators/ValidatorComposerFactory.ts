export function ValidatorComposerFactory(validators: PropertyDecorator[]) {
  return function (target: any, propertyKey: string | symbol): void {
    validators.forEach((validator) => validator(target, propertyKey));
  };
}
