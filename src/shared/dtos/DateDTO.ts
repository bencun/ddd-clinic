import { IsValidTimestamp } from '../decorators/IsValidTimestamp';

export class DateDTO {
  @IsValidTimestamp()
  date: Date;
}
