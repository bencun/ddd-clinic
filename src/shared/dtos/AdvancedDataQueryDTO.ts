import { Prisma } from '.prisma/client';
import { IsOptional, MaxLength, Max, IsInt, Min } from 'class-validator';
import { Transform } from 'class-transformer';
import { PaginationSettings } from '../types/constants';
import { PrismaOrderingParams, PrismaPaginationParams } from '../types/DataQuerySubtypes';

export class AdvancedDataQueryDTO {
  BuildPrismaPaginationParams(): Readonly<PrismaPaginationParams> {
    return {
      skip: this.skip,
      take: this.take,
    } as const;
  }

  BuildPrismaOrderingParams(): Readonly<PrismaOrderingParams> {
    return {
      orderBy: {
        [this.orderBy]: this.direction,
      },
    } as const;
  }

  @IsOptional()
  @Transform(({ value }) => value && value.trim())
  @MaxLength(100)
  text?: string;

  @IsOptional()
  orderBy?: string = 'uuid';

  @IsOptional()
  direction?: Prisma.SortOrder = Prisma.SortOrder.desc;

  @IsOptional()
  @Transform(({ value }) => +value)
  @IsInt()
  @Min(PaginationSettings.MIN_SKIP_VALUE)
  skip?: number = PaginationSettings.DEFAULT_SKIP_VALUE;

  @IsOptional()
  @Transform(({ value }) => +value)
  @IsInt()
  @Min(PaginationSettings.MIN_ITEMS_PER_PAGE)
  @Max(PaginationSettings.MAX_ITEMS_PER_PAGE)
  take?: number = PaginationSettings.DEFAULT_ITEMS_PER_PAGE;
}
