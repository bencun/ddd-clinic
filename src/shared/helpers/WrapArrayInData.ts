export function WrapArrayInData<T>(array: Array<T>) {
  return {
    data: array,
  };
}
