import { Prisma } from '@prisma/client';

export function contains(value?: string) {
  return value ? { contains: value, mode: Prisma.QueryMode.insensitive } : undefined;
}
