import { Prisma } from '.prisma/client';

export type PrismaPaginationParams = {
  skip: number;
  take: number;
};

export type PrismaOrderingParams = {
  orderBy: {
    [x: string]: Prisma.SortOrder;
  };
};
