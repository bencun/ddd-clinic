export enum PersistenceType {
  NEW = 'NEW',
  NOT_UNIQUE = 'NOT_UNIQUE',
  VALID = 'VALID',
  INVALID = 'INVALID',
}
