export type WrappedPaginationData<T> = {
  data: T[];
  count: number;
  skip: number;
  take: number;
};
