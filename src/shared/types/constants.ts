export const PaginationSettings = {
  MIN_ITEMS_PER_PAGE: 1,
  MAX_ITEMS_PER_PAGE: 50,
  DEFAULT_ITEMS_PER_PAGE: 10,
  MIN_SKIP_VALUE: 0,
  DEFAULT_SKIP_VALUE: 0,
} as const;

export enum SupportedLocales {
  en = 'en',
}
