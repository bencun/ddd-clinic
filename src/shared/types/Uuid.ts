import { isUUID } from 'class-validator';

export type Uuid = string;

export const isUuid = (value: string): value is Uuid => {
  return isUUID(value, '4');
};
