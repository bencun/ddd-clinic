import { isPhoneNumber } from 'class-validator';
import { ValueObject } from '../ddd/ValueObject';

type TPhoneNumber = {
  value: string;
};

export class PhoneNumber extends ValueObject<TPhoneNumber> {
  constructor(phoneNumber: string) {
    const trimmedValue = phoneNumber.trim();
    if (isPhoneNumber(trimmedValue)) {
      super({ value: trimmedValue });
    } else {
      throw new Error('Invalid phone number');
    }
  }
}
