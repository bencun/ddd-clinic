import { Diagnosis } from './Diagnosis';
describe('Diagnosis test', () => {
  it('should be a valid diagnosis code', () => {
    const code = 'R456';
    const name = 'Violentia physica';
    const diagnosis = new Diagnosis(code);
    expect(diagnosis.name).toEqual(name);
  });

  it('should be an invalid diagnosis code', () => {
    const code = 'Z456.352';
    expect(() => {
      new Diagnosis(code);
    }).toThrow();
  });
});
