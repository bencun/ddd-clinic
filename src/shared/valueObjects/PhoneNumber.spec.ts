import { PhoneNumber } from './PhoneNumber';

describe('PhoneNumber tests', () => {
  it('should be a valid phone number 1', () => {
    const number = '+381 1234567';
    const p = new PhoneNumber(number);
    expect(p.props.value).toEqual(number);
  });

  it('should be a valid phone number 2', () => {
    const number = '+381 065123456';
    const p = new PhoneNumber(number);
    expect(p.props.value).toEqual(number);
  });

  it('should be a valid phone number 3', () => {
    const number = '+3816 43356 783';
    const p = new PhoneNumber(number);
    expect(p.props.value).toEqual(number);
  });

  it('should be a valud phone number 4', () => {
    const number = '+3 (816) 43356 783';
    const p = new PhoneNumber(number);
    expect(p.props.value).toEqual(number);
  });

  it('should be an invalid phone number', () => {
    expect(() => {
      const number = '+38164#33453';
      new PhoneNumber(number);
    }).toThrow();
  });
});
