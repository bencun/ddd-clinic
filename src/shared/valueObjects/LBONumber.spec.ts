import { LBONumber } from './LBONumber';

describe('LBONumber test', () => {
  it('should be valid 1', () => {
    const number = '12345678910';
    const lbo = new LBONumber(number);
    expect(lbo.props.value).toEqual(number);
  });

  it('should be valid 2', () => {
    const number = '   12345678910   ';
    const lbo = new LBONumber(number);
    expect(lbo.props.value).toEqual(number.trim());
  });

  it('should be invalid 1', () => {
    const number = '   1a345678910   ';
    expect(() => {
      new LBONumber(number);
    }).toThrow();
  });

  it('should be invalid 2', () => {
    const number = '1345678910';
    expect(() => {
      new LBONumber(number);
    }).toThrow();
  });

  it('should be invalid 3', () => {
    const number = '034567891 1';
    expect(() => {
      new LBONumber(number);
    }).toThrow();
  });
});
