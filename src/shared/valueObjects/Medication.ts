import { ValueObject } from '../ddd/ValueObject';
import { MedicationSpecification } from './MedicationSpecification';

type TMedication = {
  name: string;
  spec: MedicationSpecification;
};
export class Medication extends ValueObject<TMedication> {
  constructor(name: string, spec: MedicationSpecification) {
    if (name.trim().length > 0) {
      super({ name, spec });
    } else {
      throw new Error('Invalid medication');
    }
  }
}
