import { ValueObject } from '../ddd/ValueObject';
import { Medication } from './Medication';

type TPrescription = {
  notes: string;
  medication: Medication;
};

export class Prescription extends ValueObject<TPrescription> {
  constructor(notes: string, medication: Medication) {
    if (notes.trim().length > 0) {
      super({ medication, notes });
    } else {
      throw new Error('Prescription is invalid.');
    }
  }
}
