import { ValueObject } from '../ddd/ValueObject';

type KeyValueProps<V> = {
  key: string;
  value: V;
};
export class KeyValue<V> extends ValueObject<KeyValueProps<V>> {
  constructor(private _key: string, private _value: V) {
    super({ key: _key, value: _value });
  }

  get value() {
    return this._value;
  }
}
