import { Email } from './Email';

describe('Email tests', () => {
  it('should be a valid email 1', () => {
    const email = 'name@naming.com';
    const p = new Email(email);
    expect(p.props.value).toEqual(email.trim());
  });

  it('should be a valid email 2', () => {
    const email = 'name@naming.com  ';
    const p = new Email(email);
    expect(p.props.value).toEqual(email.trim());
  });

  it('should be an invalid email 1', () => {
    expect(() => {
      const email = 'name@naming.com a';
      new Email(email);
    }).toThrow();
  });

  it('should be an invalid email 2', () => {
    expect(() => {
      const email = 'test@tests';
      new Email(email);
    }).toThrow();
  });
});
