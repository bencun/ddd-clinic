import { HealthRecord as HealthRecordPersistent } from '.prisma/client';
import { HealthRecord } from 'src/subdomains/patients/entities/HealthRecord';
import { Diagnosis } from '../Diagnosis';
import { PrescriptionsMapper, RawPrescriptionsArray } from './PrescriptionsMapper';

export class HealthRecordMapper {
  public static toValueObject(hr: HealthRecordPersistent): HealthRecord {
    return new HealthRecord(
      hr.examinationUuid,
      hr.diagnosesEstablished.map((d) => new Diagnosis(d)),
      hr.chronicDiagnosesEstablished.map((d) => new Diagnosis(d)),
      hr.chronicDiagnosesRemoved.map((d) => new Diagnosis(d)),
      (hr.prescriptions as RawPrescriptionsArray).map((p) => PrescriptionsMapper.toValueObject(p)),
      hr.version,
      hr.date,
      hr.uuid,
    );
  }
}
