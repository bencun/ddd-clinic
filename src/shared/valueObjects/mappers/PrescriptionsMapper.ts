import { Medication } from '../Medication';
import { MedicationSpecification, TDoseUnit } from '../MedicationSpecification';
import { Prescription } from '../Prescription';

export type RawPrescription = {
  medication: {
    name: string;
    spec: {
      doseUnit: TDoseUnit;
      doseAmount: number;
    };
  };
  notes: string;
};

export type RawPrescriptionsArray = RawPrescription[];

export class PrescriptionsMapper {
  static toValueObject(pr: RawPrescription): Prescription {
    return new Prescription(
      pr.notes,
      new Medication(
        pr.medication.name,
        new MedicationSpecification(pr.medication.spec.doseUnit, pr.medication.spec.doseAmount),
      ),
    );
  }

  static toPersistenceObject(pr: Prescription): RawPrescription {
    return {
      notes: pr.props.notes,
      medication: {
        name: pr.props.medication.props.name,
        spec: {
          doseAmount: pr.props.medication.props.spec.props.doseAmount,
          doseUnit: pr.props.medication.props.spec.props.doseUnit,
        },
      },
    };
  }
}
