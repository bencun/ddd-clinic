import { ValueObject } from '../ddd/ValueObject';

type TAddress = {
  city: string;
  postalCode: string;
  street: string;
  number: string;
};

export class Address extends ValueObject<TAddress> {
  constructor(city: string, postalCode: string, street: string, number: string) {
    const conditions = [
      city.trim().length >= 2,
      postalCode.trim().length >= 3,
      street.trim().length >= 1,
      number.trim().length >= 1,
    ];
    if (conditions.every((c) => c)) {
      super({
        city: city.trim(),
        postalCode: postalCode.trim(),
        street: street.trim(),
        number: number.trim(),
      });
    } else {
      throw new Error('Invalid address');
    }
  }
}
