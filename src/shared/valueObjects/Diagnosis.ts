import { DiagnosisCodesMap } from '../constants/DiagnosisCodesMap';
import { ValueObject } from '../ddd/ValueObject';

type TDiagnosis = {
  code: string;
};
export class Diagnosis extends ValueObject<TDiagnosis> {
  public readonly name: string;
  constructor(code: string) {
    const name = DiagnosisCodesMap.get(code);
    if (name) {
      super({ code });
      this.name = name;
    } else {
      throw new Error('Invalid diagnosis code.');
    }
  }
}
