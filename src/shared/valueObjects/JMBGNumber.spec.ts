import { JMBGNumber } from './JMBGNumber';

describe('JMBGNumber tests', () => {
  it('should be valid 1', () => {
    const number = '1508965123456';
    const jmbg = new JMBGNumber(number);
    expect(jmbg.props.value).toEqual(number);
  });

  it('should be valid 2', () => {
    const number = '3112001123456';
    const jmbg = new JMBGNumber(number);
    expect(jmbg.props.value).toEqual(number);
  });

  it('should be valid 3', () => {
    const number = ' 0112001123456 ';
    const jmbg = new JMBGNumber(number);
    expect(jmbg.props.value).toEqual(number.trim());
  });

  it('should be invalid 1', () => {
    const number = '3212001123456';
    expect(() => {
      new JMBGNumber(number);
    }).toThrow();
  });

  it('should be invalid 2', () => {
    const number = '3113001123456';
    expect(() => {
      new JMBGNumber(number);
    }).toThrow();
  });

  it('should be invalid 3', () => {
    const number = '311300112345';
    expect(() => {
      new JMBGNumber(number);
    }).toThrow();
  });
});
