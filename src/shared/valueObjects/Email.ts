import { isEmail } from 'class-validator';
import { ValueObject } from '../ddd/ValueObject';

type TEmail = {
  value: string;
};

export class Email extends ValueObject<TEmail> {
  constructor(email: string) {
    const trimmedValue = email.trim();
    if (isEmail(trimmedValue)) {
      super({ value: trimmedValue });
    } else {
      throw new Error('Invalid email address');
    }
  }
}
