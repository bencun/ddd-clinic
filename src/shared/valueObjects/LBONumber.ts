import { isNumberString } from 'class-validator';
import { ValueObject } from '../ddd/ValueObject';

type TLBONumber = {
  value: string;
};

export class LBONumber extends ValueObject<TLBONumber> {
  constructor(value: string) {
    const trimmedValue = value.trim();
    const isValid = trimmedValue.length === 11 && isNumberString(trimmedValue);
    if (isValid) {
      super({ value: trimmedValue });
    } else {
      throw new Error('Invalid LBO number');
    }
  }
}
