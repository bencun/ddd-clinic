import { isNumberString } from 'class-validator';
import { ValueObject } from '../ddd/ValueObject';

type TJMBGNumber = {
  value: string;
};

export class JMBGNumber extends ValueObject<TJMBGNumber> {
  private static isValid(value: string): boolean {
    const isNumeric = isNumberString(value);
    const isProperLength = value.length === 13;
    const dayIsValid = +value.substring(0, 2) <= 31;
    const monthIsValid = +value.substring(2, 4) <= 12;
    // no year validation
    return isNumeric && isProperLength && dayIsValid && monthIsValid;
  }
  constructor(value: string) {
    const trimmedValue = value.trim();
    if (JMBGNumber.isValid(trimmedValue)) {
      super({ value: trimmedValue });
    } else {
      throw new Error('Invalid JMBG');
    }
  }
}
