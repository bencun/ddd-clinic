import { Address } from './Address';

describe('Address tests', () => {
  it('should be a valid address', () => {
    const city = 'Nis';
    const postalCode = '18000';
    const street = 'Test street';
    const number = '44a';
    const a = new Address(city, postalCode, street, number);
    expect(a.props).toEqual({ city, postalCode, street, number });
  });

  it('should be an invalid address', () => {
    const city = 'Nis';
    const postalCode = '18000';
    const street = 'Test street';
    const number = '44a';
    const a = new Address(city, postalCode, street, number);
    expect(a.props).toEqual({ city, postalCode, street, number });
  });

  it('should be an invalid city', () => {
    const city = '';
    const postalCode = '18000';
    const street = 'Test street';
    const number = '44a';
    expect(() => {
      new Address(city, postalCode, street, number);
    }).toThrow();
  });

  it('should be an invalid postal code', () => {
    const city = 'Nis';
    const postalCode = '12';
    const street = 'Test street';
    const number = '44a';
    expect(() => {
      new Address(city, postalCode, street, number);
    }).toThrow();
  });

  it('should be an invalid street', () => {
    const city = 'Nis';
    const postalCode = '18000';
    const street = ' ';
    const number = '44a';
    expect(() => {
      new Address(city, postalCode, street, number);
    }).toThrow();
  });

  it('should be an invalid number', () => {
    const city = 'Nis';
    const postalCode = '18000';
    const street = 'Test street';
    const number = '';
    expect(() => {
      new Address(city, postalCode, street, number);
    }).toThrow();
  });
});
