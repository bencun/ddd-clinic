import { ValueObject } from '../ddd/ValueObject';

export type TDoseUnit = 'mg' | 'g' | 'ml';

type TMedicationSpecification = {
  doseUnit: TDoseUnit;
  doseAmount: number;
};
export class MedicationSpecification extends ValueObject<TMedicationSpecification> {
  constructor(doseUnit: TDoseUnit, doseAmount: number) {
    if (doseAmount > 0) {
      super({ doseUnit, doseAmount });
    } else {
      throw new Error('The medication specification is incorrect');
    }
  }
}
