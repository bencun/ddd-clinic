import { Injectable, OnModuleInit, OnModuleDestroy, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit, OnModuleDestroy {
  constructor(private readonly configService: ConfigService) {
    super({
      log: configService.get('PRISMA_LOGS') === 'true' ? ['error', 'info', 'query', 'warn'] : [],
      // throw a NestJS 404 when an entity isn't found
      // rejectOnNotFound: (e) => new NotFoundException(`[PERSISTENCE ERROR] ${e}`),
    });
  }
  async onModuleInit() {
    await this.connectHidePasswordMiddleware();
    await this.connectSoftDeleteMiddleware();
    await this.$connect();
  }

  async onModuleDestroy() {
    await this.$disconnect();
  }

  async connectHidePasswordMiddleware() {
    this.$use(async (params, next) => {
      if (params.model === 'User') {
        if (
          params.action === 'findUnique' ||
          params.action === 'findMany' ||
          params.action === 'findFirst'
        ) {
          // when there are NO select params that actually means: "select all" so we do that but remove the password field
          // flaw: requires a manual maintenance of the list of the fields if we change the model!
          // note: Prisma team might actually add the "exclude" list so we will be able to fix this more easily
          if (!params.args['select']) {
            params.args['select'] = {
              uuid: true,
              email: true,
              name: true,
              phone: true,
              password: false,
              resetToken: false,
              role: true,
              deleted: true,
              createdAt: true,
              updatedAt: true,
              activated: true,
            };
          }
          // if we do have any select params defined use them all AND check if the 'password' is explicitly requested
          else if (params.args['select']) {
            params.args['select'] = {
              // preserve all the selects...
              ...params.args['select'],
              // ...and the password ONLY IF explicitly stated to be true
              password: params.args['select'].password || false,
              resetToken: params.args['select'].resetToken || false,
            };
          }
        }
      }
      // go to the next middleware
      return next(params);
    });
  }

  async connectSoftDeleteMiddleware() {
    /***********************************/
    /* SOFT DELETE MIDDLEWARE */
    /***********************************/
    /** When selecting Users */
    this.$use(async (params, next) => {
      if (params.model === 'User') {
        if (params.action === 'findUnique') {
          params.action = 'findFirst';
          params.args.where['deleted'] = false;
        }
        if (params.action === 'findMany') {
          if (params.args.where !== undefined) {
            if (params.args.where.deleted === undefined) {
              params.args.where['deleted'] = false;
            }
          }
        }
      }
      return next(params);
    });

    /** When updating Users */
    this.$use(async (params, next) => {
      if (params.model === 'User') {
        /* We have to disable this for now - updating a targeted deleted record is fine.
        if (params.action === 'update') {
          params.action = 'updateMany';
          params.args.where['deleted'] = false;
        } */
        if (params.action === 'updateMany') {
          if (params.args.where !== undefined) {
            params.args.where['deleted'] = false;
          } else {
            params.args['where'] = { deleted: false };
          }
        }
      }
      return next(params);
    });

    /** When deleting User */
    this.$use(async (params, next) => {
      if (params.model === 'User') {
        if (params.action === 'delete') {
          params.action = 'update';
          params.args['data'] = { deleted: true };
        }
        if (params.action === 'deleteMany') {
          params.action = 'updateMany';
          if (params.args.data !== undefined) {
            params.args.data['deleted'] = true;
          } else {
            params.args['data'] = { deleted: true };
          }
        }
      }
      return next(params);
    });
  }
}
