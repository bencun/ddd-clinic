import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import cookieParser from 'cookie-parser';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import fs from 'fs';
import { resolve } from 'path';
import helmet from 'helmet';

async function bootstrap() {
  let httpsOptions;
  if (process.env.NODE_ENV === 'production') {
    httpsOptions = {
      key: fs.readFileSync(resolve(`${require.main.path}/../`, `${process.env.KEY_NAME}`)),
      cert: fs.readFileSync(resolve(`${require.main.path}/../`, `${process.env.CERT_NAME}`)),
    };
  } else {
    httpsOptions = undefined; /* {
      key: fs.readFileSync(resolve(`${require.main.path}/../../`, 'localhost.key')),
      cert: fs.readFileSync(resolve(`${require.main.path}/../../`, 'localhost.crt')),
    } */
  }
  const app = await NestFactory.create(AppModule, { httpsOptions });
  // Middleware to enable proper cookie parsing
  app.use(cookieParser());
  app.use(helmet());
  if (process.env.NODE_ENV === 'production') {
    app.enableCors({
      credentials: true,
      origin: [
        /* TODO add origins for CORS */
      ],
    });
  }
  // Here we specify a global transformation and validation pipe
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
      enableDebugMessages: process.env.NODE_ENV !== 'production',
    }),
  );

  // Swagger setup
  const config = new DocumentBuilder()
    .setTitle('DDD Clinic API')
    .setDescription('The DDD Clinic API documentation')
    .setVersion('1.0')
    .addTag('ddd-clinic')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  // start listening for incoming requests
  await app.listen(process.env.APP_PORT);
}

const environment = process.env.NODE_ENV;
if (!environment) {
  console.error('Environment not specified, exiting.');
} else {
  console.info('Bootstrapping DDD Clinic for:', process.env.NODE_ENV);
  bootstrap();
}
