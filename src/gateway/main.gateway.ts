import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server } from 'http';
import { Socket } from 'socket.io';
import { AuthService } from 'src/auth/auth.service';
import { parse } from 'cookie';
import { RequestUser } from 'src/auth/types/RequestUser';
import { Uuid } from 'src/shared/types/Uuid';

@WebSocketGateway({
  cors: { credentials: true, origin: ['http://localhost:4000' /* add more domains */] },
})
export class MainGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  private server: Server;

  private subscribers: { socket: Socket; user: RequestUser }[] = [];

  constructor(private readonly authService: AuthService) {}

  async handleConnection(socket: Socket) {
    console.log('Socket connecting...', socket.id);
    try {
      const cookie = socket.handshake.headers.cookie;
      const token = parse(cookie).Authentication;
      const user = await this.authService.validateCredentialsForWebsocket(token);
      this.subscribers.push({ socket, user });
      console.log('Socket connected.', this.subscribers.length);
    } catch (e) {
      console.log('Socket error', e);
      socket.disconnect(true);
    }
  }

  handleDisconnect(socket: Socket) {
    this.subscribers = this.subscribers.filter((sub) => sub.socket !== socket);
    console.log('Socket disconnected', socket.id, this.subscribers.length);
  }

  send(userUuid: Uuid, event: string, data: unknown) {
    const sub = this.subscribers.find((sub) => sub.user.uuid === userUuid);
    if (sub) {
      sub.socket.emit(event, data);
    }
  }

  sendToAll(event: string, data: unknown) {
    this.subscribers.forEach((sub) => sub.socket.emit(event, data));
  }
}
