import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { IEventHandler } from 'src/shared/ddd/IEventHandler';
import { PatientProfileUpdatedEvent } from 'src/subdomains/patients/events/PatientProfileUpdatedEvent';
import { MainGateway } from '../main.gateway';

@Injectable()
export class PatientProfileUpdatedSocketHandler
  implements IEventHandler<PatientProfileUpdatedEvent>
{
  constructor(private readonly gateway: MainGateway) {}

  @OnEvent(DomainEventToken.PATIENT_PROFILE_UPDATED)
  async handle(event: PatientProfileUpdatedEvent): Promise<any> {
    this.gateway.sendToAll(event.token, {
      patientUuid: event.patientUuid,
    });
  }
}
