import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { IEventHandler } from 'src/shared/ddd/IEventHandler';
import { AppointmentStartedEvent } from 'src/subdomains/scheduling/events/AppointmentStartedEvent';
import { MainGateway } from '../main.gateway';

@Injectable()
export class AppointmentStartedSocketHandler implements IEventHandler<AppointmentStartedEvent> {
  constructor(private readonly gateway: MainGateway) {}

  @OnEvent(DomainEventToken.APPOINTMENT_STARTED)
  async handle(event: AppointmentStartedEvent): Promise<any> {
    this.gateway.send(event.practitionerUuid, event.token, {
      appointmentUuid: event.appointment.uuid,
    });
  }
}
