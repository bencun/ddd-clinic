import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { IEventHandler } from 'src/shared/ddd/IEventHandler';
import { ExaminationCanceledEvent } from 'src/subdomains/examinations/events/ExaminationCanceledEvent';
import { ExaminationCompletedEvent } from 'src/subdomains/examinations/events/ExaminationCompletedEvent';
import { MainGateway } from '../main.gateway';

@Injectable()
export class ExaminationCanceledSocketHandler implements IEventHandler<ExaminationCanceledEvent> {
  constructor(private readonly gateway: MainGateway) {}

  @OnEvent(DomainEventToken.EXAMINATION_CANCELED)
  async handle(event: ExaminationCanceledEvent): Promise<any> {
    this.gateway.send(event.examination.practitionerUuid, event.token, {
      examinationUuid: event.examination.uuid,
    });
  }
}

@Injectable()
export class ExaminationCompletedSocketHandler implements IEventHandler<ExaminationCompletedEvent> {
  constructor(private readonly gateway: MainGateway) {}

  @OnEvent(DomainEventToken.EXAMINATION_COMPLETED)
  async handle(event: ExaminationCompletedEvent): Promise<any> {
    this.gateway.send(event.examination.practitionerUuid, event.token, {
      examinationUuid: event.examination.uuid,
    });
  }
}
