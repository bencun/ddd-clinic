import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { DomainEventToken } from 'src/shared/constants/DomainEventToken';
import { MainGateway } from '../main.gateway';

@Injectable()
export class ScheduleUpdatedSocketHandler {
  constructor(private readonly gateway: MainGateway) {}

  private _handle() {
    this.gateway.sendToAll(DomainEventToken.SCHEDULE_UPDATED, {});
  }

  @OnEvent(DomainEventToken.APPOINTMENT_CREATED)
  createdHandle() {
    this._handle();
  }

  @OnEvent(DomainEventToken.APPOINTMENT_UPDATED)
  updatedHandle() {
    this._handle();
  }

  @OnEvent(DomainEventToken.APPOINTMENT_STARTED)
  startedHandle() {
    this._handle();
  }

  @OnEvent(DomainEventToken.APPOINTMENT_FINISHED)
  finishedHandle() {
    this._handle();
  }

  @OnEvent(DomainEventToken.APPOINTMENT_CANCELED)
  canceledHandle() {
    this._handle();
  }
}
