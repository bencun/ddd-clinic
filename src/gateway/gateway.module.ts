import { Module } from '@nestjs/common';
import { AppointmentStartedSocketHandler } from './handlers/AppointmentStartedSocketHandler';
import {
  ExaminationCanceledSocketHandler,
  ExaminationCompletedSocketHandler,
} from './handlers/ExaminationConcludedHandlers';
import { PatientProfileUpdatedSocketHandler } from './handlers/PatientProfileUpdatedSocketHandler';
import { ScheduleUpdatedSocketHandler } from './handlers/ScheduleUpdatedSocketHandler';
import { MainGateway } from './main.gateway';

@Module({
  providers: [
    MainGateway,
    AppointmentStartedSocketHandler,
    ScheduleUpdatedSocketHandler,
    PatientProfileUpdatedSocketHandler,
    ExaminationCanceledSocketHandler,
    ExaminationCompletedSocketHandler,
  ],
})
export class GatewayModule {}
