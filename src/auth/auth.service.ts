import {
  BadRequestException,
  Injectable,
  OnModuleInit,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/database/prisma.service';
import { JWT_ACCESS_TOKEN_NAME, JWT_REFRESH_TOKEN_NAME } from './types/constants';
import bcrypt from 'bcrypt';
import { RequestUser } from './types/RequestUser';
import { User } from '@prisma/client';
import { TokenPair } from './types/TokenPair';
import { InvalidTokenException } from './types/InvalidTokenException';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ResetPasswordDTO } from 'src/users/activation/dtos/ResetPasswordDTO';
import { WsException } from '@nestjs/websockets';
import { JWTPayload } from './types/JWTPayload';

@Injectable()
export class AuthService implements OnModuleInit {
  constructor(
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly prisma: PrismaService,
  ) {}

  async hashPassword(rawPassword: string) {
    return await bcrypt.hash(rawPassword, 10);
  }

  /**
   * This lifecycle hook is tasked with creating the initial admin user
   */
  async onModuleInit() {
    const shouldInitUser = this.configService.get<boolean>('INIT_USER');
    const email = this.configService.get<string>('INIT_USER_EMAIL');
    const name = this.configService.get<string>('INIT_USER_NAME');
    const phone = this.configService.get<string>('INIT_USER_PHONE');
    const rawPassword = this.configService.get<string>('INIT_USER_PASSWORD');

    console.info(`Checking if we should try to init the user: ${shouldInitUser}`);
    if (shouldInitUser) {
      // seed the initial admin user if necessary
      const count = await this.prisma.user.count({
        where: { email: { equals: email } },
      });
      console.info(`User already in the DB: ${count === 1}`);
      if (count === 0) {
        const password = await this.hashPassword(rawPassword);
        console.info(`New user data: ${email} : <password hidden>`);
        await this.prisma.user.create({
          data: {
            email,
            name,
            phone,
            password,
            role: 'ADMIN',
            activated: true,
          },
        });
      }
    }
  }

  /**
   * Validates the email and password
   * @param email
   * @param password
   */
  async validateCredentials(email: string, password: string): Promise<RequestUser> {
    // communicate with the DB
    try {
      const partialUser = await this.prisma.user.findUnique({
        select: {
          email: true,
          password: true,
          activated: true,
        },
        where: {
          email: email,
        },
      });
      if (partialUser && partialUser.activated) {
        const passwordIsValid = await bcrypt.compare(password, partialUser.password);
        if (passwordIsValid) {
          return await this.prisma.user.findUnique({ where: { email } });
        }
      }
    } catch (e) {}
    return null;
  }

  /**
   * Validates the email and password
   * @param email
   * @param password
   */
  async validateCredentialsForWebsocket(token: string): Promise<RequestUser> {
    // token validation
    const payload: JWTPayload = await this.jwtService.verifyAsync(token);
    if (!payload?.sub) {
      throw new WsException('Invalid credentials.');
    }
    // communicate with the DB
    try {
      const partialUser = await this.prisma.user.findUnique({
        select: {
          activated: true,
        },
        where: {
          uuid: payload.sub,
        },
      });
      if (partialUser && partialUser.activated) {
        return await this.prisma.user.findUnique({ where: { uuid: payload.sub } });
      }
    } catch (e) {
      throw new WsException('Valid user not found.');
    }
  }

  /**
   * Validates the existence of an access token in the token whitelist.
   */
  async validateAccessToken(token: string) {
    return !!(await this.prisma.token.findFirst({
      where: {
        authentication: token,
      },
    }));
  }
  /**
   * Finds one active user by uuid.
   */
  async findActiveUserById(uuid: User['uuid']) {
    return await this.prisma.user.findFirst({
      where: {
        uuid,
        activated: true,
      },
    });
  }

  /**
   * Validates the existing token pairs
   * and generates and stores a new valid token pair in the DB.
   */
  async getNewValidTokenPair(userId: User['uuid'], oldTokenPair?: TokenPair) {
    // find the old user tokens if needed
    if (oldTokenPair) {
      // if the token pair is found first invalidate and then proceed
      try {
        await this.invalidateAuthTokenPair(oldTokenPair);
      } catch (e) {
        // If the old token pair wasn't found that means that the pair that is used to generate the new token pair
        // was stolen and is therefore insecure and we need to invalidate all of the user's currently active tokens
        // to prevent any further usage of the potentially stolen tokens.
        await this.invalidateAllUserTokenPairs(userId);
        throw new UnauthorizedException();
      }
    }

    // Start generating new tokens
    // jwt payload for the tokens
    const payload = { sub: userId };
    // generate tokens and related data
    const refreshTokenExpiresIn = this.configService.get<number>('JWT_REFRESH_TOKEN_MAX_AGE');
    const authTokenExpiresIn = this.configService.get<number>('JWT_ACCESS_TOKEN_MAX_AGE');
    const refreshTokenExpiryTimestamp = new Date(Date.now() + refreshTokenExpiresIn * 1000);
    const refreshToken = await this.jwtService.signAsync(
      { ...payload },
      {
        secret: this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
        expiresIn: `${refreshTokenExpiresIn}s`,
      },
    );
    const authorizationToken = await this.jwtService.signAsync(
      { ...payload },
      {
        secret: this.configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
        expiresIn: `${authTokenExpiresIn}s`,
      },
    );

    console.info('Storing for user', userId);
    // store the newly generated tokens in the DB
    await this.prisma.token.create({
      data: {
        owner: {
          connect: {
            uuid: userId,
          },
        },
        authentication: authorizationToken,
        refresh: refreshToken,
        expiresAt: refreshTokenExpiryTimestamp,
      },
    });

    // generate cookies
    return [
      this.generateHttpOnlyCookie(JWT_REFRESH_TOKEN_NAME, refreshToken, refreshTokenExpiresIn),
      this.generateHttpOnlyCookie(JWT_ACCESS_TOKEN_NAME, authorizationToken, authTokenExpiresIn),
    ];
  }

  /**
   * Invalidates a known auth token pair
   */
  async invalidateAuthTokenPair({ refresh, authentication }: TokenPair) {
    const token = await this.prisma.token.findFirst({
      where: {
        authentication,
        refresh,
      },
    });

    if (token) {
      // if the token pair exists invalidate it
      await this.prisma.token.delete({
        where: {
          uuid: token.uuid,
        },
      });
    } else {
      // otherwise we throw an error
      throw new InvalidTokenException();
    }
  }

  /**
   * Invalidates all of the User's token pairs
   */
  async invalidateAllUserTokenPairs(userId: User['uuid']) {
    await this.prisma.token.deleteMany({
      where: {
        ownerUuid: userId,
      },
    });
  }

  /**
   * Generates and returns an array of empty auth cookies
   */
  public getEmptyAuthCookies() {
    return [
      this.generateHttpOnlyCookie(JWT_ACCESS_TOKEN_NAME, '', 0),
      this.generateHttpOnlyCookie(JWT_REFRESH_TOKEN_NAME, '', 0),
    ];
  }

  /**
   * Generates and returns a cookie.
   */
  private generateHttpOnlyCookie(name: string, value: string, maxAge: number) {
    return `${name}=${value}; HttpOnly; Path=/; Max-Age=${maxAge}`;
  }

  async changeUserPassword(userUuid: User['uuid'], resetPasswordDto: ResetPasswordDTO) {
    // verify that the passwords match
    if (resetPasswordDto.password !== resetPasswordDto.passwordConfirm) {
      throw new BadRequestException();
    }
    // update password
    const updatedUser = await this.prisma.user.update({
      where: { uuid: userUuid },
      data: {
        password: await this.hashPassword(resetPasswordDto.password),
        resetToken: null,
      },
    });
    return updatedUser;
  }

  @Cron(CronExpression.EVERY_HOUR)
  private async purgeExpiredTokens() {
    const result = await this.prisma.token.deleteMany({
      where: {
        expiresAt: {
          lt: new Date(Date.now()),
        },
      },
    });
    console.info(`Cron job complete. Purged ${result.count} token(s).`);
  }
}
