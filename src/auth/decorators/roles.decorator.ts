import { Role } from '.prisma/client';
import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { RolesGuard } from '../guards/roles.guard';

export const ROLES_KEY = 'ROLES_KEY';

const RolesDecorator = (...roles: Role[]) => SetMetadata(ROLES_KEY, roles);

export function Roles(...roles: Role[]) {
  return applyDecorators(RolesDecorator(...roles), UseGuards(RolesGuard));
}
