import { SetMetadata } from '@nestjs/common';

export const ROUTE_IS_PUBLIC = 'isPublic';

/**
 * Marks the route as public so that the JWT access token auth strategy is ignored.
 */
export const PublicRoute = () => SetMetadata(ROUTE_IS_PUBLIC, true);
