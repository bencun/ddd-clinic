import { Global, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { LocalStrategy } from './strategies/local-strategy.provider';
import { JwtAccessTokenStrategy } from './strategies/jwt-access-token-strategy.provider';
import { JwtRefreshTokenStrategy } from './strategies/jwt-refresh-token-strategy.provider';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { JwtAccessTokenAuthGuard } from './guards/jwt-access-token-auth.guard';

@Global()
@Module({
  controllers: [AuthController],
  imports: [
    // register the Passport module
    PassportModule,

    // register the JwtModule to be able to access the JwtService
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) =>
        // this object sets up the default signing and verification config for the JwtService
        ({
          secret: configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
          signOptions: {
            expiresIn: configService.get<number>('JWT_ACCESS_TOKEN_MAX_AGE'),
          },
        } as JwtModuleOptions),
    }),
  ],
  providers: [
    AuthService,
    LocalStrategy,
    JwtAccessTokenStrategy,
    JwtRefreshTokenStrategy,
    // register the JwtAccessTokenAuthGuard globally for all routes by default
    { provide: APP_GUARD, useClass: JwtAccessTokenAuthGuard },
  ],
  exports: [AuthService],
})
export class AuthModule {}
