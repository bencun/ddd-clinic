import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';
import { AuthService } from '../auth.service';
import { JWT_ACCESS_TOKEN_NAME } from '../types/constants';
import { JWTPayload } from '../types/JWTPayload';
import { RequestUser } from '../types/RequestUser';
import { Request } from 'express';

@Injectable()
export class JwtAccessTokenStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request) => {
          return request?.cookies?.[JWT_ACCESS_TOKEN_NAME];
        },
      ]),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
      passReqToCallback: true,
    } as StrategyOptions);
  }

  /**
   * A function invoked by Passport after the JWT token has been decoded
   * by the jwtFromRequest function defined in the constructor options.
   * @param payload
   */
  async validate(request: Request, payload: JWTPayload): Promise<RequestUser> {
    console.info('Calling the JWT Access Token Strategy validate function...');
    // validate the token against the whitelist
    console.info('Validating token', request.cookies?.[JWT_ACCESS_TOKEN_NAME]);
    const tokenValid = await this.authService.validateAccessToken(
      request.cookies?.[JWT_ACCESS_TOKEN_NAME],
    );
    console.info('Token valid:', tokenValid);
    if (!tokenValid) {
      throw new UnauthorizedException();
    }
    // grab the user
    const user = await this.authService.findActiveUserById(payload.sub);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
