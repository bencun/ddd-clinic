import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';
import { AuthService } from '../auth.service';
import { JWT_REFRESH_TOKEN_NAME, JWT_REFRESH_TOKEN_STRATEGY } from '../types/constants';
import { JWTPayload } from '../types/JWTPayload';
import { RequestUser } from '../types/RequestUser';

@Injectable()
export class JwtRefreshTokenStrategy extends PassportStrategy(
  Strategy,
  JWT_REFRESH_TOKEN_STRATEGY,
) {
  constructor(
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (request) => {
          return request?.cookies?.[JWT_REFRESH_TOKEN_NAME];
        },
      ]),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
    } as StrategyOptions);
  }

  /**
   * A function invoked by Passport after the JWT token has been decoded
   * by the jwtFromRequest function defined in the constructor options.
   * This function also validates the access token against the token whitelist.
   * @param payload
   */
  async validate(payload: JWTPayload): Promise<RequestUser> {
    console.info('Calling the JWT Refresh Token Strategy validate function...');
    const user = await this.authService.findActiveUserById(payload.sub);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
