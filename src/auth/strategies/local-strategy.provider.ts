import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { IStrategyOptionsWithRequest, Strategy } from 'passport-local';
import { AuthService } from '../auth.service';
import { RequestUser } from '../types/RequestUser';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      usernameField: 'email',
      passwordField: 'password',
    } as IStrategyOptionsWithRequest);
  }

  /**
   * Automatically invoked by Passport when trying to log in the user.
   * Validates the username and password through the AuthService.
   */
  async validate(email: string, password: string): Promise<RequestUser> {
    console.info('Calling the Local Strategy validate function...', email, password);
    const user = await this.authService.validateCredentials(email, password);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
