import { Request } from 'express';
import { RequestUser } from '../types/RequestUser';

interface RequestWithUser extends Request {
  user: RequestUser;
}

export default RequestWithUser;
