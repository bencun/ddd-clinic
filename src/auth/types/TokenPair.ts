export type TokenPair = {
  authentication: string;
  refresh: string;
};
