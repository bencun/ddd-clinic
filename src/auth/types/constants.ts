import { Role } from '.prisma/client';

export const JWT_ACCESS_TOKEN_NAME = 'Authentication';
export const JWT_REFRESH_TOKEN_NAME = 'Refresh';
export const JWT_REFRESH_TOKEN_STRATEGY = 'jwt-refresh-token';
export const ALL_ROLES: Role[] = [Role.ADMIN, Role.PRACTITIONER, Role.NURSE];
