import { Body, Controller, Get, Post, Req, Res, UseGuards } from '@nestjs/common';
import RequestWithUser from './interfaces/request-with-user.interface';
import { Response } from 'express';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { JwtRefreshTokenAuthGuard } from './guards/jwt-refresh-token-auth.guard';
import { PublicRoute } from './decorators/public-route.decorator';
import { TokenPair } from './types/TokenPair';
import { JWT_ACCESS_TOKEN_NAME, JWT_REFRESH_TOKEN_NAME } from './types/constants';
import { LoginDTO } from './dtos/LoginDTO';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  /**
   * Returns the current token pair from the request.
   */
  private getCurrentTokenPair(request: RequestWithUser) {
    // grab the current token pair
    const requestTokenPair: TokenPair = {
      authentication: request.cookies?.[JWT_ACCESS_TOKEN_NAME],
      refresh: request.cookies?.[JWT_REFRESH_TOKEN_NAME],
    };
    return requestTokenPair;
  }
  /**
   * Mutates the request and attaches the appropriate auth cookies.
   */
  private async attachAuthCookiesToResponse(
    request: RequestWithUser,
    response: Response,
    useOldTokenPairToGenerateNewPair = true,
  ) {
    // grab the user
    const { user } = request;
    // grab the token pair
    const requestTokenPair = this.getCurrentTokenPair(request);
    const oldTokenPair = useOldTokenPairToGenerateNewPair && requestTokenPair;

    // generate new tokens
    const newTokens = await this.authService.getNewValidTokenPair(user.uuid, oldTokenPair);
    response.setHeader('Set-Cookie', newTokens);
  }

  /**
   * Mutates the request and clears all auth cookies.
   */
  private async clearAuthCookiesInResponse(response: Response) {
    response.setHeader('Set-Cookie', this.authService.getEmptyAuthCookies());
  }

  @PublicRoute()
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(
    @Req() request: RequestWithUser,
    @Res({ passthrough: true }) response: Response,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @Body() body: LoginDTO,
  ) {
    console.info('Login endpoint hit', request.user.email);
    await this.attachAuthCookiesToResponse(request, response, false);
    return request.user;
  }

  @Get('logout')
  async logout(@Req() request: RequestWithUser, @Res({ passthrough: true }) response: Response) {
    await this.authService.invalidateAuthTokenPair(this.getCurrentTokenPair(request));
    await this.clearAuthCookiesInResponse(response);
  }

  // do not use the access token here since we only need the refresh token!
  @PublicRoute()
  @UseGuards(JwtRefreshTokenAuthGuard)
  @Get('refresh')
  async refresh(@Req() request: RequestWithUser, @Res({ passthrough: true }) response: Response) {
    console.info('Refresh Token endpoint hit', request.user);
    await this.attachAuthCookiesToResponse(request, response);
    return request.user;
  }
}
