import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JWT_REFRESH_TOKEN_STRATEGY } from '../types/constants';

@Injectable()
export class JwtRefreshTokenAuthGuard extends AuthGuard(JWT_REFRESH_TOKEN_STRATEGY) {}
