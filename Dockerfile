#args set globally
ARG APP_PORT
ARG KEY_NAME
ARG CERT_NAME

FROM node:14-alpine AS BUILD_IMAGE

# Create app directory
WORKDIR /usr/src/app

ARG APP_PORT
ARG KEY_NAME
ARG CERT_NAME
ENV APP_PORT "$APP_PORT"
ENV KEY_NAME "$KEY_NAME"
ENV CERT_NAME "$CERT_NAME"

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY *.json ./
COPY .env ./
COPY prisma ./prisma
COPY src ./src
COPY test ./test
COPY ${KEY_NAME} ./
COPY ${CERT_NAME} ./
RUN npm install
# If you are building your code for production
# RUN npm ci --only=production
# Bundle app source

RUN npm run build
# remove development dependencies
RUN npm prune --production

FROM node:14-alpine

WORKDIR /usr/src/app

ARG APP_PORT
ARG KEY_NAME
ARG CERT_NAME
ENV APP_PORT "$APP_PORT"
ENV KEY_NAME "$KEY_NAME"
ENV CERT_NAME "$CERT_NAME"

# copy from build image
COPY --from=BUILD_IMAGE /usr/src/app/*.json ./
COPY --from=BUILD_IMAGE /usr/src/app/.env ./.env
COPY --from=BUILD_IMAGE /usr/src/app/prisma ./prisma
COPY --from=BUILD_IMAGE /usr/src/app/dist ./dist
COPY --from=BUILD_IMAGE /usr/src/app/node_modules ./node_modules
COPY --from=BUILD_IMAGE /usr/src/app/${KEY_NAME} ./dist/${KEY_NAME}
COPY --from=BUILD_IMAGE /usr/src/app/${CERT_NAME} ./dist/${CERT_NAME}
EXPOSE ${APP_PORT}
CMD [ "npm", "run", "start:prod"]