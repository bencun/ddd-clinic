import { Role, PrismaClient } from '@prisma/client';
import faker from 'faker';
import bcrypt from 'bcrypt';

/** Prisma client for seeding */
const prisma = new PrismaClient();

/** Does something N times */
const times = async <T>(n: number, f: () => Promise<T>) => {
  return await Promise.all(
    Array(n)
      .fill(undefined)
      .map(async () => await f()),
  );
};

/** Controls the seeding process */
async function main() {
  // users
  await prisma.user.createMany({
    data: [
      {
        name: 'Name Nurse',
        email: 'nurse@mail.com',
        password: bcrypt.hashSync('password', 10),
        phone: '+3812345678',
        role: Role.NURSE,
        activated: true,
      },
      {
        name: 'Name Practitioner',
        email: 'practitioner@mail.com',
        password: bcrypt.hashSync('password', 10),
        phone: '+3818765432',
        role: Role.PRACTITIONER,
        activated: true,
      },
    ],
  });
  // patients
  for (let i = 0; i < 31; i++) {
    await prisma.patient.create({
      data: {
        name: `${faker.name.firstName()} ${faker.name.middleName()} ${faker.name.lastName()}`,
        email: faker.internet.email(),
        city: faker.address.city(),
        postalCode: faker.address.zipCode(),
        street: faker.address.streetName(),
        number: faker.address.streetAddress(),
        jmbg: `${i < 10 ? `0${i}` : i}07993123456`,
        lbo: `${i < 10 ? `0${i}` : i}123456789`,
        phone: faker.phone.phoneNumber('+381 6########'),
      },
    });
  }
}

/** Entrypoint function, might be exported in the future instead of being ran on module load */
async function seed() {
  try {
    await main();
    process.exit(0);
  } catch (e) {
    console.error('Error:', e);
    process.exit(1);
  }
}
// go
seed();
