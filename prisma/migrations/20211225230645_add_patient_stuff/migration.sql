-- CreateTable
CREATE TABLE "Patient" (
    "uuid" UUID NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "jmbg" CHAR(13) NOT NULL,
    "lbo" CHAR(11) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "phone" VARCHAR(255) NOT NULL,
    "city" VARCHAR(255) NOT NULL,
    "postalCode" VARCHAR(255) NOT NULL,
    "street" VARCHAR(255) NOT NULL,
    "number" VARCHAR(255) NOT NULL,

    CONSTRAINT "Patient_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "HealthRecord" (
    "uuid" UUID NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "patientUuid" UUID NOT NULL,
    "date" TIMESTAMP NOT NULL,
    "examinationUuid" UUID NOT NULL,
    "diagnosesEstablished" TEXT[],
    "chronicDiagnosesEstablished" TEXT[],
    "chronicDiagnosesRemoved" TEXT[],
    "prescriptions" JSON NOT NULL,

    CONSTRAINT "HealthRecord_pkey" PRIMARY KEY ("uuid")
);

-- AddForeignKey
ALTER TABLE "HealthRecord" ADD CONSTRAINT "HealthRecord_patientUuid_fkey" FOREIGN KEY ("patientUuid") REFERENCES "Patient"("uuid") ON DELETE RESTRICT ON UPDATE CASCADE;
