-- AlterTable
ALTER TABLE "HealthRecord" ADD COLUMN     "version" INTEGER NOT NULL DEFAULT 1;

-- AlterTable
ALTER TABLE "Patient" ADD COLUMN     "version" INTEGER NOT NULL DEFAULT 1;

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "version" INTEGER NOT NULL DEFAULT 1;

-- DropEnum
DROP TYPE "Day";

-- CreateTable
CREATE TABLE "Appointment" (
    "uuid" UUID NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "version" INTEGER NOT NULL DEFAULT 1,
    "patientUuid" UUID NOT NULL,
    "patientName" VARCHAR(255) NOT NULL,
    "start" TIMESTAMP NOT NULL,
    "duration" INTEGER NOT NULL,
    "status" VARCHAR(255) NOT NULL,

    CONSTRAINT "Appointment_pkey" PRIMARY KEY ("uuid")
);
