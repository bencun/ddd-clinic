/*
  Warnings:

  - A unique constraint covering the columns `[jmbg]` on the table `Patient` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[lbo]` on the table `Patient` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateTable
CREATE TABLE "Examination" (
    "uuid" UUID NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "version" INTEGER NOT NULL DEFAULT 1,
    "appointmentUuid" UUID NOT NULL,
    "practitionerUuid" UUID NOT NULL,
    "patientUuid" UUID NOT NULL,
    "patientName" VARCHAR(255) NOT NULL,
    "knownChronicDiagnoses" TEXT[],
    "startedAt" TIMESTAMP(3) NOT NULL,
    "endedAt" TIMESTAMP(3),
    "status" VARCHAR(255) NOT NULL,
    "anamnesis" TEXT NOT NULL,
    "notes" TEXT NOT NULL,
    "diagnoses" TEXT[],
    "chronicDiagnosesEstablished" TEXT[],
    "chronicDiagnosesRemoved" TEXT[],
    "prescriptions" JSON NOT NULL,

    CONSTRAINT "Examination_pkey" PRIMARY KEY ("uuid")
);

-- CreateIndex
CREATE UNIQUE INDEX "Patient_jmbg_key" ON "Patient"("jmbg");

-- CreateIndex
CREATE UNIQUE INDEX "Patient_lbo_key" ON "Patient"("lbo");
