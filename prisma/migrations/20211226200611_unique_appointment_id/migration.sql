/*
  Warnings:

  - A unique constraint covering the columns `[appointmentUuid]` on the table `Examination` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Examination_appointmentUuid_key" ON "Examination"("appointmentUuid");
