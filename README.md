# DDD Clinic Backend
## Description

DDD Clinic backend implementation in NestJS.

## Installation

### Prerequisites
`Node >= 16.13.x`

`npm >= 6.x`

**Do not use NPM 7.x since `node-gyp` has issues with it and it's used internally by some of our packages.**

Visual Studio Code as a preferred editor.

Additionally, please install the `ESLint` extension and the `Prettier` extension in VSCode.

Please set up the extensions to lint and fix on save by adding this to your `Workspace Preferences`:
```json
{
  // ...your other settings
	"editor.codeActionsOnSave": {
		"source.fixAll": true,
	}
}
```
### Installing the dependencies
```bash
$ npm install
```
## Running the app locally
Create a `.env` file using the provided `.env.example` file.

We support 3 .env files: `.local.env`, `.development.env`, `.env`. The loading is prioritized in this order.

First run the DB:
```bash
$ docker-compose -f "docker-compose.development.yml" up -d
```

Then run the Prisma migrations and the seed if you need to:
```bash
$ npm run migrations:dev
$	npm run db:seed
```

Then run the NestJS app:
```bash
# Development
$ npm run start

# Watch mode
$ npm run start:dev

# Production mode
$ npm run start:prod
```
## Production deployment
TBA WIP

```

## Prisma NPM scripts
- `migrations:dev` - Use in development if you really want to. Basically only used when you want to generate a new migration. Creates a new migration if schema was modified. Runs all of the migrations to the database. Generates a new client.
- `migrations:reset` - Use in development only. Doesn't generate a new migration. Removes everything from the DB, re-runs the existing migrations. Generates a new client. Seeds the database automatically.
- `db:push` - Use in development. Pushes the schema directly to the DB without any migrations-related stuff.
- `db:seed` - Use in development. Only seeds the database.


## CERTIFICATES - local development

localhost.crt
localhost.key 

Generated for local development only.